﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace GeoMapping.Data
{
    public static class PredicateBuilder
    {
        public static Expression<Func<T, bool>> True<T>() { return f => true; }
        public static Expression<Func<T, bool>> False<T>() { return f => false; }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1,
                                                            Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>
                  (Expression.OrElse(expr1.Body, invokedExpr), expr1.Parameters);
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1,
                                                             Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>
                  (Expression.AndAlso(expr1.Body, invokedExpr), expr1.Parameters);
        }

        public static Expression<Func<T, bool>> CreateEqualSingleExpression<T>(string property, object value)
        {
            Expression<Func<T, bool>> result = null;
            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            if (p.Type.GetProperty(property) != null)
            {
                Expression propertyExpression = Expression.Property(p, property);
                var propertyType = Nullable.GetUnderlyingType(propertyExpression.Type) ?? propertyExpression.Type;
                propertyExpression = Expression.Convert(propertyExpression, propertyType);
                if (value is IEnumerable<object>)
                {
                    var valueLst = value as JArray;
                    var predicate = False<T>();
                    foreach (var item in valueLst)
                    {
                        var valueWithType = propertyType.IsEnum ? Enum.ToObject(propertyType, ((JValue)item).Value) : Convert.ChangeType(((JValue)item).Value, propertyType);
                        //p.Property == value
                        var equalsExpression = Expression.Equal(propertyExpression, Expression.Constant(valueWithType));

                        //p => p.Property == value
                        var lambda = Expression.Lambda<Func<T, bool>>(equalsExpression, p);
                        predicate = predicate.Or(lambda);
                    }
                    result = predicate;
                }
                else
                {
                    var valueWithType = propertyType.IsEnum ? Enum.ToObject(propertyType, value) : Convert.ChangeType(value, propertyType);
                    //var valueWithType = Convert.ChangeType(value, propertyType);
                    var equalsExpression = Expression.Equal(propertyExpression, Expression.Constant(valueWithType));
                    //p => p.Property == value
                    result = Expression.Lambda<Func<T, bool>>(equalsExpression, p);
                }
            }
            return result;
        }

        public static Expression<Func<T, bool>> CreateContainsExpression<T>(string property, string value)
        {
            Expression<Func<T, bool>> result = null;
            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            if (p.Type.GetProperty(property) != null)
            {
                Expression propertyExpression = Expression.Property(p, property);
                var propertyType = Nullable.GetUnderlyingType(propertyExpression.Type) ?? propertyExpression.Type;
                propertyExpression = Expression.Convert(propertyExpression, propertyType);

                MethodInfo method = typeof(string).GetMethod("Contains", new[] { typeof(string) });

                var containsExpression = Expression.Call(propertyExpression, method, Expression.Constant(Convert.ChangeType(value, propertyType)));
                //p => p.Property == value
                result = Expression.Lambda<Func<T, bool>>(containsExpression, p);

            }
            return result;
        }

        public static Expression<Func<T, bool>> CreateGreaterThanOrLessThanSingleExpression<T>(string property, object value)
        {
            Expression<Func<T, bool>> result = null;
            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            if (p.Type.GetProperty(property) != null)
            {
                Expression propertyExpression = Expression.Property(p, property);
                var propertyType = Nullable.GetUnderlyingType(propertyExpression.Type) ?? propertyExpression.Type;
                propertyExpression = Expression.Convert(propertyExpression, propertyType);

                if (value is IEnumerable<object>)
                {
                    var valueLst = value as JArray;
                    var predicate = PredicateBuilder.False<T>();
                    JToken firstValue = valueLst.First();
                    var greaterThanExpr = Expression.GreaterThanOrEqual(propertyExpression, Expression.Constant(Convert.ChangeType(((JValue)firstValue).Value, propertyType)));
                    var lambdaGreater = Expression.Lambda<Func<T, bool>>(greaterThanExpr, p);
                    predicate = predicate.Or(lambdaGreater);

                    JToken secondValue = valueLst.Skip(1).Take(1).First();
                    var lessThanExpr = Expression.LessThanOrEqual(propertyExpression, Expression.Constant(Convert.ChangeType(((JValue)secondValue).Value, propertyType)));
                    var lambdaLess = Expression.Lambda<Func<T, bool>>(lessThanExpr, p);
                    result = predicate.And(lambdaLess);

                }
                //else
                //{
                //    var equalsExpression = Expression.GreaterThan(propertyExpression, Expression.Constant(Convert.ChangeType(value, propertyType)));
                //    //p => p.Property == value
                //    result = Expression.Lambda<Func<T, bool>>(equalsExpression, p);
                //}
            }
            return result;
        }
    }
}
