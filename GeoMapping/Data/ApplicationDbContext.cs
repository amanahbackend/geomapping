﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using GeoMapping.Models;
using GeoMapping.Models.Configurations;

namespace GeoMapping.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Privilege> Privileges { get; set; }
        public DbSet<RolePrivilege> RolePrivileges { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<Plot> Plots { get; set; }
        public DbSet<Place> Places { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<SubStatus> SubStatuses { get; set; }
        public DbSet<Competitor> Competitors { get; set; }
        public DbSet<SurveyHistory> SurveyHistories { get; set; }
        public DbSet<Track> Tracks { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<PinSections> PinSections { get; set; }
        public DbSet<PlotType> PlotTypes { get; set; }
        public DbSet<Block> Blocks { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Segment> Segments { get; set; }
        public DbSet<SubSegment> SubSegments { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new PrivilegeTypeConfiguration());
            builder.ApplyConfiguration(new RolePrivilegeTypeConfiguration());

            builder.Entity<Plot>().HasIndex(p => p.PlotKey).IsUnique();
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<GeoMapping.Models.ApplicationUser> ApplicationUser { get; set; }

        public DbSet<GeoMapping.Models.Role> Role { get; set; }
    }
}
