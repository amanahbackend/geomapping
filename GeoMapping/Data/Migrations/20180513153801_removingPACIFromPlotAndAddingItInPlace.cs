﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GeoMapping.Data.Migrations
{
    public partial class removingPACIFromPlotAndAddingItInPlace : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PACI_NO",
                table: "Plots");

            migrationBuilder.AddColumn<string>(
                name: "PACINo",
                table: "Places",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PACINo",
                table: "Places");

            migrationBuilder.AddColumn<string>(
                name: "PACI_NO",
                table: "Plots",
                nullable: true);
        }
    }
}
