﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GeoMapping.Data.Migrations
{
    public partial class add_assign_agent_fk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssignedAgent",
                table: "Plots");

            migrationBuilder.AddColumn<string>(
                name: "AssignedAgentId",
                table: "Plots",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Plots_AssignedAgentId",
                table: "Plots",
                column: "AssignedAgentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Plots_AspNetUsers_AssignedAgentId",
                table: "Plots",
                column: "AssignedAgentId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plots_AspNetUsers_AssignedAgentId",
                table: "Plots");

            migrationBuilder.DropIndex(
                name: "IX_Plots_AssignedAgentId",
                table: "Plots");

            migrationBuilder.DropColumn(
                name: "AssignedAgentId",
                table: "Plots");

            migrationBuilder.AddColumn<string>(
                name: "AssignedAgent",
                table: "Plots",
                nullable: true);
        }
    }
}
