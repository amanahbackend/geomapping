﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GeoMapping.Data.Migrations
{
    public partial class addingBrandSectionRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SectionId",
                table: "Brands",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Brands_SectionId",
                table: "Brands",
                column: "SectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Brands_Sections_SectionId",
                table: "Brands",
                column: "SectionId",
                principalTable: "Sections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Brands_Sections_SectionId",
                table: "Brands");

            migrationBuilder.DropIndex(
                name: "IX_Brands_SectionId",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "SectionId",
                table: "Brands");
        }
    }
}
