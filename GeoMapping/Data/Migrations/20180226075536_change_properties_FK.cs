﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GeoMapping.Data.Migrations
{
    public partial class change_properties_FK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PinSections_SurveyHistories_SurveyHistoryId",
                table: "PinSections");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "SurveyHistories");

            migrationBuilder.DropColumn(
                name: "HistoryId",
                table: "PinSections");

            migrationBuilder.AlterColumn<int>(
                name: "SurveyHistoryId",
                table: "PinSections",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PinSections_SurveyHistories_SurveyHistoryId",
                table: "PinSections",
                column: "SurveyHistoryId",
                principalTable: "SurveyHistories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PinSections_SurveyHistories_SurveyHistoryId",
                table: "PinSections");

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "SurveyHistories",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SurveyHistoryId",
                table: "PinSections",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "HistoryId",
                table: "PinSections",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_PinSections_SurveyHistories_SurveyHistoryId",
                table: "PinSections",
                column: "SurveyHistoryId",
                principalTable: "SurveyHistories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
