﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GeoMapping.Data.Migrations
{
    public partial class addingBrandModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsGhanim",
                table: "PinSections");

            migrationBuilder.AddColumn<int>(
                name: "BrandId",
                table: "PinSections",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Brands",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brands", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PinSections_BrandId",
                table: "PinSections",
                column: "BrandId");

            migrationBuilder.AddForeignKey(
                name: "FK_PinSections_Brands_BrandId",
                table: "PinSections",
                column: "BrandId",
                principalTable: "Brands",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PinSections_Brands_BrandId",
                table: "PinSections");

            migrationBuilder.DropTable(
                name: "Brands");

            migrationBuilder.DropIndex(
                name: "IX_PinSections_BrandId",
                table: "PinSections");

            migrationBuilder.DropColumn(
                name: "BrandId",
                table: "PinSections");

            migrationBuilder.AddColumn<bool>(
                name: "IsGhanim",
                table: "PinSections",
                nullable: false,
                defaultValue: false);
        }
    }
}
