﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GeoMapping.Data.Migrations
{
    public partial class Places_Remove_cascade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Places_Areas_AreaId",
                table: "Places");

            migrationBuilder.DropIndex(
                name: "IX_Places_AreaId",
                table: "Places");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Places_AreaId",
                table: "Places",
                column: "AreaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Areas_AreaId",
                table: "Places",
                column: "AreaId",
                principalTable: "Areas",
                principalColumn: "Id");
        }
    }
}
