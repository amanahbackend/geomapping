﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GeoMapping.Data.Migrations
{
    public partial class add_statues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "Places",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubStatusId",
                table: "Places",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Statuses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Statuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SubStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StatusId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubStatuses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubStatuses_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Places_StatusId",
                table: "Places",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Places_SubStatusId",
                table: "Places",
                column: "SubStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_SubStatuses_StatusId",
                table: "SubStatuses",
                column: "StatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Statuses_StatusId",
                table: "Places",
                column: "StatusId",
                principalTable: "Statuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Places_SubStatuses_SubStatusId",
                table: "Places",
                column: "SubStatusId",
                principalTable: "SubStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Places_Statuses_StatusId",
                table: "Places");

            migrationBuilder.DropForeignKey(
                name: "FK_Places_SubStatuses_SubStatusId",
                table: "Places");

            migrationBuilder.DropTable(
                name: "SubStatuses");

            migrationBuilder.DropTable(
                name: "Statuses");

            migrationBuilder.DropIndex(
                name: "IX_Places_StatusId",
                table: "Places");

            migrationBuilder.DropIndex(
                name: "IX_Places_SubStatusId",
                table: "Places");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "Places");

            migrationBuilder.DropColumn(
                name: "SubStatusId",
                table: "Places");
        }
    }
}
