﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GeoMapping.Data.Migrations
{
    public partial class addingBlockModelAndModifyingRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plots_Areas_AreaId",
                table: "Plots");

            migrationBuilder.DropIndex(
                name: "IX_Plots_AreaId",
                table: "Plots");

            migrationBuilder.DropColumn(
                name: "AreaId",
                table: "Plots");

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Plots",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BlockId",
                table: "Plots",
                type: "int",
                nullable: true,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "LicenseNumber",
                table: "Plots",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LicenseType",
                table: "Plots",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerName",
                table: "Plots",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PlotTypeId",
                table: "Plots",
                type: "int",
                nullable: true,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BlockId",
                table: "Places",
                type: "int",
                nullable: true,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Blocks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AreaId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PACIBlockId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Polygon = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blocks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Blocks_Areas_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Areas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlotTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlotTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Plots_BlockId",
                table: "Plots",
                column: "BlockId");

            migrationBuilder.CreateIndex(
                name: "IX_Plots_PlotTypeId",
                table: "Plots",
                column: "PlotTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Places_BlockId",
                table: "Places",
                column: "BlockId");

            migrationBuilder.CreateIndex(
                name: "IX_Blocks_AreaId",
                table: "Blocks",
                column: "AreaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Blocks_BlockId",
                table: "Places",
                column: "BlockId",
                principalTable: "Blocks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Plots_Blocks_BlockId",
                table: "Plots",
                column: "BlockId",
                principalTable: "Blocks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Plots_PlotTypes_PlotTypeId",
                table: "Plots",
                column: "PlotTypeId",
                principalTable: "PlotTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Places_Blocks_BlockId",
                table: "Places");

            migrationBuilder.DropForeignKey(
                name: "FK_Plots_Blocks_BlockId",
                table: "Plots");

            migrationBuilder.DropForeignKey(
                name: "FK_Plots_PlotTypes_PlotTypeId",
                table: "Plots");

            migrationBuilder.DropTable(
                name: "Blocks");

            migrationBuilder.DropTable(
                name: "PlotTypes");

            migrationBuilder.DropIndex(
                name: "IX_Plots_BlockId",
                table: "Plots");

            migrationBuilder.DropIndex(
                name: "IX_Plots_PlotTypeId",
                table: "Plots");

            migrationBuilder.DropIndex(
                name: "IX_Places_BlockId",
                table: "Places");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "Plots");

            migrationBuilder.DropColumn(
                name: "BlockId",
                table: "Plots");

            migrationBuilder.DropColumn(
                name: "LicenseNumber",
                table: "Plots");

            migrationBuilder.DropColumn(
                name: "LicenseType",
                table: "Plots");

            migrationBuilder.DropColumn(
                name: "OwnerName",
                table: "Plots");

            migrationBuilder.DropColumn(
                name: "PlotTypeId",
                table: "Plots");

            migrationBuilder.DropColumn(
                name: "BlockId",
                table: "Places");

            migrationBuilder.AddColumn<int>(
                name: "AreaId",
                table: "Plots",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Plots_AreaId",
                table: "Plots",
                column: "AreaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Plots_Areas_AreaId",
                table: "Plots",
                column: "AreaId",
                principalTable: "Areas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
