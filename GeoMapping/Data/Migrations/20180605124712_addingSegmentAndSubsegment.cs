﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GeoMapping.Data.Migrations
{
    public partial class addingSegmentAndSubsegment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SegmentId",
                table: "Places",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubSegmentId",
                table: "Places",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Segments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Segments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SubSegments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubSegments", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Places_SegmentId",
                table: "Places",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Places_SubSegmentId",
                table: "Places",
                column: "SubSegmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Segments_SegmentId",
                table: "Places",
                column: "SegmentId",
                principalTable: "Segments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Places_SubSegments_SubSegmentId",
                table: "Places",
                column: "SubSegmentId",
                principalTable: "SubSegments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Places_Segments_SegmentId",
                table: "Places");

            migrationBuilder.DropForeignKey(
                name: "FK_Places_SubSegments_SubSegmentId",
                table: "Places");

            migrationBuilder.DropTable(
                name: "Segments");

            migrationBuilder.DropTable(
                name: "SubSegments");

            migrationBuilder.DropIndex(
                name: "IX_Places_SegmentId",
                table: "Places");

            migrationBuilder.DropIndex(
                name: "IX_Places_SubSegmentId",
                table: "Places");

            migrationBuilder.DropColumn(
                name: "SegmentId",
                table: "Places");

            migrationBuilder.DropColumn(
                name: "SubSegmentId",
                table: "Places");
        }
    }
}
