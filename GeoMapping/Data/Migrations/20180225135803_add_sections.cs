﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GeoMapping.Data.Migrations
{
    public partial class add_sections : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SurveyHistories_Competitors_CompetitorId",
                table: "SurveyHistories");

            migrationBuilder.DropForeignKey(
                name: "FK_SurveyHistories_SubStatuses_SubStatusId",
                table: "SurveyHistories");

            migrationBuilder.DropIndex(
                name: "IX_SurveyHistories_CompetitorId",
                table: "SurveyHistories");

            migrationBuilder.DropColumn(
                name: "CompetitorId",
                table: "SurveyHistories");

            migrationBuilder.DropColumn(
                name: "IsGhanim",
                table: "SurveyHistories");

            migrationBuilder.AlterColumn<int>(
                name: "SubStatusId",
                table: "SurveyHistories",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateTable(
                name: "PinSections",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Competitor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsAgreed = table.Column<bool>(type: "bit", nullable: false),
                    IsGhanim = table.Column<bool>(type: "bit", nullable: false),
                    PlaceId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    SurveyHistoryId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PinSections", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PinSections_SurveyHistories_SurveyHistoryId",
                        column: x => x.SurveyHistoryId,
                        principalTable: "SurveyHistories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Sections",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sections", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PinSections_SurveyHistoryId",
                table: "PinSections",
                column: "SurveyHistoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_SurveyHistories_SubStatuses_SubStatusId",
                table: "SurveyHistories",
                column: "SubStatusId",
                principalTable: "SubStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SurveyHistories_SubStatuses_SubStatusId",
                table: "SurveyHistories");

            migrationBuilder.DropTable(
                name: "PinSections");

            migrationBuilder.DropTable(
                name: "Sections");

            migrationBuilder.AlterColumn<int>(
                name: "SubStatusId",
                table: "SurveyHistories",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompetitorId",
                table: "SurveyHistories",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsGhanim",
                table: "SurveyHistories",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_SurveyHistories_CompetitorId",
                table: "SurveyHistories",
                column: "CompetitorId");

            migrationBuilder.AddForeignKey(
                name: "FK_SurveyHistories_Competitors_CompetitorId",
                table: "SurveyHistories",
                column: "CompetitorId",
                principalTable: "Competitors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SurveyHistories_SubStatuses_SubStatusId",
                table: "SurveyHistories",
                column: "SubStatusId",
                principalTable: "SubStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
