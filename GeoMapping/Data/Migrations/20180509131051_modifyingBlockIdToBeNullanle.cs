﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GeoMapping.Data.Migrations
{
    public partial class modifyingBlockIdToBeNullanle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Places_Blocks_BlockId",
                table: "Places");

            migrationBuilder.DropForeignKey(
                name: "FK_Plots_Blocks_BlockId",
                table: "Plots");

            migrationBuilder.AlterColumn<int>(
                name: "BlockId",
                table: "Plots",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "BlockId",
                table: "Places",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Blocks_BlockId",
                table: "Places",
                column: "BlockId",
                principalTable: "Blocks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Plots_Blocks_BlockId",
                table: "Plots",
                column: "BlockId",
                principalTable: "Blocks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Places_Blocks_BlockId",
                table: "Places");

            migrationBuilder.DropForeignKey(
                name: "FK_Plots_Blocks_BlockId",
                table: "Plots");

            migrationBuilder.AlterColumn<int>(
                name: "BlockId",
                table: "Plots",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BlockId",
                table: "Places",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Blocks_BlockId",
                table: "Places",
                column: "BlockId",
                principalTable: "Blocks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Plots_Blocks_BlockId",
                table: "Plots",
                column: "BlockId",
                principalTable: "Blocks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
