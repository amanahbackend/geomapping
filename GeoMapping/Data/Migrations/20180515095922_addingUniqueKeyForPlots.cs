﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GeoMapping.Data.Migrations
{
    public partial class addingUniqueKeyForPlots : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plots_PlotTypes_PlotTypeId",
                table: "Plots");

            migrationBuilder.AlterColumn<int>(
                name: "PlotTypeId",
                table: "Plots",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "PlotKey",
                table: "Plots",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Plots_PlotKey",
                table: "Plots",
                column: "PlotKey",
                unique: true,
                filter: "[PlotKey] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Plots_PlotTypes_PlotTypeId",
                table: "Plots",
                column: "PlotTypeId",
                principalTable: "PlotTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plots_PlotTypes_PlotTypeId",
                table: "Plots");

            migrationBuilder.DropIndex(
                name: "IX_Plots_PlotKey",
                table: "Plots");

            migrationBuilder.DropColumn(
                name: "PlotKey",
                table: "Plots");

            migrationBuilder.AlterColumn<int>(
                name: "PlotTypeId",
                table: "Plots",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Plots_PlotTypes_PlotTypeId",
                table: "Plots",
                column: "PlotTypeId",
                principalTable: "PlotTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
