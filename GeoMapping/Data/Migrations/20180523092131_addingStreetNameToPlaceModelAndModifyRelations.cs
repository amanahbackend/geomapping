﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GeoMapping.Data.Migrations
{
    public partial class addingStreetNameToPlaceModelAndModifyRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Places_Areas_AreaId",
                table: "Places");

            migrationBuilder.DropForeignKey(
                name: "FK_Places_Plots_PlotId",
                table: "Places");

            migrationBuilder.AlterColumn<int>(
                name: "PlotId",
                table: "Places",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "AreaId",
                table: "Places",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "StreetName",
                table: "Places",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Areas_AreaId",
                table: "Places",
                column: "AreaId",
                principalTable: "Areas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Plots_PlotId",
                table: "Places",
                column: "PlotId",
                principalTable: "Plots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Places_Areas_AreaId",
                table: "Places");

            migrationBuilder.DropForeignKey(
                name: "FK_Places_Plots_PlotId",
                table: "Places");

            migrationBuilder.DropColumn(
                name: "StreetName",
                table: "Places");

            migrationBuilder.AlterColumn<int>(
                name: "PlotId",
                table: "Places",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AreaId",
                table: "Places",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Areas_AreaId",
                table: "Places",
                column: "AreaId",
                principalTable: "Areas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Plots_PlotId",
                table: "Places",
                column: "PlotId",
                principalTable: "Plots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
