﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Spatial;
using System.Threading.Tasks;

namespace GeoMapping.Models
{
    public class Area
    {
		public int Id{ get; set; }
		public string Name{ get; set; }
        public string AreaNo { get; set; }
        public string Polygon { get; set; }
        public string GovObjectId { get; set; }
    }
}
