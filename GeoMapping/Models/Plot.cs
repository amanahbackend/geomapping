﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models
{
    public class Plot
    {
        public int Id { get; set; }
        [Display(Name = "Plot Name")]
        public string Name { get; set; }
        public string Polygon { get; set; }
        [Display(Name = "PAHW No.")]
        public string PAHW_NO { get; set; }
        [Display(Name = "Block Name")]
        public int? BlockId { get; set; }
        public Block Block { get; set; }
        public string AssignedAgentId { get; set; }
        public ApplicationUser AssignedAgent { get; set; }

        // New Fields
        public string OwnerName { get; set; }
        public string Address { get; set; }
        public string LicenseType { get; set; }
        public string LicenseNumber { get; set; }
        public int? PlotTypeId { get; set; }
        public PlotType PlotType { get; set; }

        public int? PlotKey { get; set; }
    }
}
