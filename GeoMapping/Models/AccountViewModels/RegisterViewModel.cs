﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        public string Id { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

		[Required]
		[StringLength(8, ErrorMessage = "Phone number should be {1} numbers long.", MinimumLength = 8)]
		[Display(Name = "Phone Number")]
		public string PhoneNumber { get; set; }

		[Required]
		[Display(Name = "User Name")]
		[StringLength(50, ErrorMessage = "The user name must be at least {2} and the minimum {1} characters long.", MinimumLength = 6)]
		public string UserName { get; set; }

		[Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

		[Display(Name = "User Role")]
		[Required]
		public string RoleId { get; set; }
	}
}
