﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models
{
    public class Place
    {
        public int Id { get; set; }
        [Display(Name = "Place Name")]
        public string Name { get; set; }
        public string Point { get; set; }
        [Display(Name = "Area Name")]
        public int? AreaId { get; set; }
        public Area Area { get; set; }
        [Display(Name = "Block Name")]
        public int? BlockId { get; set; }
        public Block Block { get; set; }
        [Display(Name = "Plot Name")]
        public int? PlotId { get; set; }
        public Plot Plot { get; set; }
        [Display(Name = "Street Name")]
        public string StreetName { get; set; }
        public int? StatusId { get; set; }
        public int? SubStatusId { get; set; }
        public Status Status { get; set; }
        public SubStatus SubStatus { get; set; }
        [Display(Name = "PACI Number")]
        public string PACINo { get; set; }
        
        [Display(Name = "Segment")]
        public int? SegmentId{ get; set; }
        public Segment Segment { get; set; }

        [Display(Name = "SubSegment")]
        public int? SubSegmentId { get; set; }
        public SubSegment SubSegment { get; set; }
    }
}
