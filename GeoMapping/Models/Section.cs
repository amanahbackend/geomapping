﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models
{
    public class Section
    {
		public int Id{ get; set; }
		public string Name { get; set; }
        public List<Brand> Brands { get; set; }
    }
}
