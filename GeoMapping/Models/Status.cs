﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models
{
    public class Status
    {
		public int Id{ get; set; }
		public string Name { get; set; }
		//public List<SubStatus> SubStatuses { get; set; }
	}
}
