﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models
{
    public class PinSections
    {
		public int Id{ get; set; }
		public int SectionId{ get; set; }
		public bool IsAgreed{ get; set; }
		public string Competitor { get; set; }
		public int SurveyHistoryId{ get; set; }
		public int PlaceId{ get; set; }
        public int? BrandId { get; set; }
        public Brand Brand { get; set; }
    }
}
