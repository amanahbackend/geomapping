﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models
{
    public class Privilege
    {
		public int Id { get; set; }
		public string Name { get; set; }
        [NotMapped]
		public List<Role> Roles { get; set; }
	}
}
