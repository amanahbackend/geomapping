﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models
{
	public class SurveyHistory
	{
		public int Id { get; set; }
		public int PlaceId { get; set; }
		public int StatusId { get; set; }
		public int? SubStatusId { get; set; }
		public string CreatedUserId { get; set; }
		public DateTime CreatedDate { get; set; }
		public List<PinSections> Sections {get;set;}
		public Status Status{ get; set; }
		public SubStatus SubStatus{ get; set; }
		public ApplicationUser CreatedUser { get; set; }
		public Place Place { get; set; }
	}
}
