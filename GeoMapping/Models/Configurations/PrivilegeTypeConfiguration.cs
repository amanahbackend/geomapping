﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.Configurations
{
	public class PrivilegeTypeConfiguration
  : IEntityTypeConfiguration<Privilege>
	{
		public void Configure(EntityTypeBuilder<Privilege> builder)
		{
			builder.ToTable("Privilege");
			builder.Property(p => p.Id).UseSqlServerIdentityColumn();
			builder.Property(p => p.Name).IsRequired(true);
			builder.Ignore(p => p.Roles);
		}
	}
}