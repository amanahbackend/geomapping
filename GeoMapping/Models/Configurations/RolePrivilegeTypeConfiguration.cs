﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.Configurations
{
	public class RolePrivilegeTypeConfiguration
		   : IEntityTypeConfiguration<RolePrivilege>
	{
		public void Configure(EntityTypeBuilder<RolePrivilege> builder)
		{
			builder.ToTable("RolePrivilege");
			builder.Property(p => p.Id).UseSqlServerIdentityColumn();
			builder.Property(p => p.Fk_Privilege_Id).IsRequired(true);
			builder.Property(p => p.Fk_ApplicationRole_Id).IsRequired(true);
		}
	}
}
