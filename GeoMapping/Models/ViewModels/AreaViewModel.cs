﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Spatial;
using System.Threading.Tasks;

namespace GeoMapping.Models.ViewModels
{
    public class AreaViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Point> Polygon { get; set; }
        public string GovObjectId { get; set; }
        public object AreaNo { get; set; }
    }
}