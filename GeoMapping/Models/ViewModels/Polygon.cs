﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.ViewModels
{
    public class Polygon
    {
		public List<Point> Points { get; set; }
	}
}
