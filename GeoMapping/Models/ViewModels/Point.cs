﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.ViewModels
{
    public class Point
    {
		public decimal Lat { get; set; }
		public decimal Lng { get; set; }
	}
}
