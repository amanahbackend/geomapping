﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.ViewModels
{
    public class AssignPlotViewModel
    {
		[Display(Name = "Agent Name")]
		public string AgentId { get; set; }
		[Display(Name = "Plot Name")]
		public int PlotId { get; set; }
	}
}
