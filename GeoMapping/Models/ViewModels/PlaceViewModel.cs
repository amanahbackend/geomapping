﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.ViewModels
{
	public class PlaceViewModel
	{
		public int? Id { get; set; }
		public string Name { get; set; }
		public string AreaName { get; set; }
        public string BlockName { get; set; }
        public string PlotName { get; set; }
        public string PACINo { get; set; }
        public string StatusName { get; set; }
        public Point Points { get; set; }
        public string SegmentName { get; set; }
        public string SubsegmentName { get; set; }
    }
}
