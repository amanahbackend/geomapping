﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.ViewModels
{
    public class PinSectionViewModel
    {
		public int SectionId { get; set; }
		public bool IsAgreed { get; set; }
		public bool IsGhanim { get; set; }
		public string Competitor { get; set; }
	}
}
