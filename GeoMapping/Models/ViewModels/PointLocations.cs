﻿using System;

namespace GeoMapping.Models.ViewModels
{
	public class PointLocations
	{
		public DateTime CreatedDate{ get; set; }
		public Point Location{ get; set; }
	}
}