﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.ViewModels
{
    public class UserViewModel
    {
		public string Id { get; set; }
		public string RoleId { get; set; }
		public string RoleName { get; set; }
		public string UserName { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
	}
}