﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.ViewModels
{
    public class PlaceAPIViewModel
    {
		public string PlaceName { get; set; }
		public string PACI { get; set; }
		public string PAHW { get; set; }
		public decimal Lng { get; set; }
		public decimal Lat { get; set; }
	}
}
