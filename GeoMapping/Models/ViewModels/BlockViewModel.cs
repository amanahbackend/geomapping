﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.ViewModels
{
    public class BlockViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public string PACIBlockId { get; set; }
        public List<Point> Polygon { get; set; }
    }
}
