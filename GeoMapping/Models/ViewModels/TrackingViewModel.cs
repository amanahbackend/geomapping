﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.ViewModels
{
    public class TrackingViewModel
    {
		public int Id { get; set; }
		public List<PointLocations> Tracks { get; set; }
		public string AgentId { get; set; }
		public string AgentName{ get; set; }
	}
}
