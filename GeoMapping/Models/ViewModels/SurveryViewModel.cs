﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.ViewModels
{
    public class SurveryViewModel
    {
		public int PlaceId { get; set; }
		public int StatusId { get; set; }
		public int? SubStatusId { get; set; }
		//public string UserId { get; set; }
		public List<PinSections> Sections { get; set; }
        public int Id { get; set; }
        public string PlaceName { get; set; }
        public string StatusName { get; set; }
        public string UserName { get; set; }
        public string CreatedDate { get; set; }
    }
}
