﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.ViewModels
{
    public class PlotViewModel/*:AreaViewModel*/
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Point> Polygon { get; set; }
        public string BlockName { get; set; }
        public int BlockId { get; set; }
        public string PaciNo{ get; set; }
        public string PahwNo { get; set; }
        public int PlacesNo { get; set; }
        public string OwnerName { get; set; }
        public string Address { get; set; }
        public string LicenseType { get; set; }
        public string LicenseNumber { get; set; }
        public string PlotTypeName { get; set; }
        public int? PlotKey { get; set; }
        public int AreaId { get; set; }
    }
}
