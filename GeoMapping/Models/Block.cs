﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models
{
    public class Block
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AreaId { get; set; }
        public Area Area { get; set; }
        public string PACIBlockId { get; set; }
        public string Polygon { get; set; }
    }
}
