﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models
{
	public class RolePrivilege
	{
		public int Id { get; set; }
		public string Fk_ApplicationRole_Id { get; set; }
		public int Fk_Privilege_Id { get; set; }
	}
}
