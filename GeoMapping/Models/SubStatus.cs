﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models
{
    public class SubStatus
    {
		public int Id{ get; set; }
		public string  Name{ get; set; }
		public int StatusId{ get; set; }
		public Status Status { get; set; }
	}
}
