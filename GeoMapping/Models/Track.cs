﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models
{
    public class Track
    {
		public int Id{ get; set; }
		public DateTime CreatedDate { get; set; }
		public decimal  Lat{ get; set; }
		public decimal  Long{ get; set; }
		public string  AgentId{ get; set; }
		public ApplicationUser Agent{ get; set; }
	}
}
