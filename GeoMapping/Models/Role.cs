﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models
{
    public class Role:IdentityRole
    {
        [NotMapped]
        public List<Privilege> Privileges { get; set; }
    }
}