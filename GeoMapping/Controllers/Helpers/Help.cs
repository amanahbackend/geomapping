﻿using GeoMapping.Data;
using GeoMapping.Models;
using GeoMapping.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GeoMapping.Controllers.Helpers
{
    public static class Help
    {

        public static IEnumerable<ApplicationUser> GetAgents(ApplicationDbContext _context)
        {
            var agentRole = _context.Roles.Where(x => x.Name == "Agent").FirstOrDefault();
            if (agentRole != null)
            {
                List<string> agentIds = _context.UserRoles.Where(x => x.RoleId == agentRole.Id).Select(x => x.UserId).ToList();
                return _context.ApplicationUser.Where(x => agentIds.Contains(x.Id));
            }
            return null;
        }
        public static string GetUserId(ClaimsPrincipal User)
        {
            return User.FindFirst("jti")?.Value;
        }
        public static string ToPointDbStructure(Point point)
        {
            var p = string.Format("{0}lat{0}:{1},{0}lng{0}:{2}", '"', point.Lat, point.Lng);
            return "{" + p + "}";
        }

        public static string ToPolygonDbStructure(List<List<List<double>>> polygon)
        {
            var p = "[";
            for (int i = 0; i < polygon.Count; i++)
            {
                for (int j = 0; j < polygon[i].Count; j++)
                {
                    p += "{ " + '"' + "lat" + '"' + " :" + '"' + polygon[i][j][1] + '"' + ", " + '"' + "lng" + '"' + " :" + '"' + polygon[i][j][1] + '"' + " }";
                    if (j != polygon[i].Count - 1)
                    {
                        p += ",";
                    }
                }
                if (i != polygon.Count - 1)
                {
                    p += ",";
                }
            }
            p += "]";
            return p;
        }
    }
}
