﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeoMapping.Data;
using GeoMapping.Models;
using GeoMapping.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace GeoMapping.Controllers
{
    [Authorize]
    [Route("Blocks")]
    public class BlocksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BlocksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Blocks
        [HttpGet]
        [Route("Index")]
        public ActionResult Index()
        {
            var models = new List<BlockViewModel>();
            var blocks = _context.Blocks.Include(x => x.Area).ToList();
            foreach (var item in blocks)
            {
                if (item.Polygon != "Random")
                {
                    var model = new BlockViewModel()
                    {
                        AreaName = item.Area.Name,
                        Id = item.Id,
                        Name = item.Name,
                        PACIBlockId = item.PACIBlockId,
                        Polygon = JsonConvert.DeserializeObject<List<Point>>(item.Polygon)
                    };
                    models.Add(model);
                }
            }
            return View(models);
        }

        [HttpPost]
        [Route("AddPolygon")]
        public bool AddPolygon(string name, string areaId, string polygon, string paciBlockId)
        {
            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(polygon))
            {
                Block block = new Block()
                {
                    Name = name,
                    Polygon = polygon,
                    AreaId = Convert.ToInt32(areaId),
                    PACIBlockId = paciBlockId
                };
                _context.Blocks.Add(block);
                _context.SaveChanges();
                //return RedirectToAction(nameof(Index));
                return true;
            }
            return false;
        }

        // GET: Blocks/Details/5
        [HttpGet]
        [Route("Details")]
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Blocks/Create
        [HttpGet]
        [Route("Create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Blocks/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Create")]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Blocks/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Edit")]
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Blocks/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Blocks/Delete/5
        [HttpGet]
        [Route("Delete")]
        public ActionResult Delete(int id)
        {
            var block = _context.Blocks.Include(x => x.Area).FirstOrDefault(x => x.Id == id);
            if (block != null)
            {
                BlockViewModel blockViewModel = new BlockViewModel
                {
                    AreaId = block.AreaId,
                    AreaName = block.Area.Name,
                    Id = block.Id,
                    Name = block.Name,
                    PACIBlockId = block.PACIBlockId,
                    Polygon = JsonConvert.DeserializeObject<List<Point>>(block.Polygon)
                };
                return View(blockViewModel);
            }
            else
            {
                return NotFound();
            }
        }

        // POST: Blocks/Delete/5
        [HttpPost, ActionName("Delete"), Route("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id/*, IFormCollection collection*/)
        {
            try
            {
                // TODO: Add delete logic here
                var block = _context.Blocks.FirstOrDefault(x => x.Id == id);
                _context.Blocks.Remove(block);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        [Route("GetBlockById/{id}")]
        public BlockViewModel GetBlockById(int id)
        {
            var block = _context.Blocks.Include(x => x.Area).Where(x => x.Id == id).FirstOrDefault();

            BlockViewModel blockViewModel = new BlockViewModel()
            {
                Id = block.Id,
                Name = block.Name,
                PACIBlockId = block.PACIBlockId,
                AreaId = block.AreaId,
                AreaName = block.Area.Name,
                Polygon = JsonConvert.DeserializeObject<List<Point>>(block.Polygon)
            };
            return blockViewModel;
        }

        [HttpGet]
        [Route("GetByAreaId/{id}")]
        public List<BlockViewModel> GetByAreaId(int id)
        {
            var blocks = _context.Blocks.Include(x => x.Area).Where(x => x.AreaId == id).ToList();
            var blockVMs = new List<BlockViewModel>();
            foreach (var item in blocks)
            {
                BlockViewModel blockViewModel = new BlockViewModel()
                {
                    Id = item.Id,
                    Name = item.Name,
                    PACIBlockId = item.PACIBlockId,
                    AreaId = item.AreaId,
                    AreaName = item.Area.Name,
                    Polygon = JsonConvert.DeserializeObject<List<Point>>(item.Polygon)
                };
                blockVMs.Add(blockViewModel);
            }

            return blockVMs;
        }

        [HttpGet]
        [Route("GetCount")]
        public int GetCount()
        {
            return _context.Blocks.Count();
        }

        [HttpGet]
        [Route("GetMarketSize")]
        public Dictionary<string, List<KeyValuePair<string, int>>> GetMarketSize(int areaId)
        {
            var blocks = _context.Blocks.Where(x => x.AreaId == areaId).ToList();
            var brands = _context.Brands.ToList();
            Dictionary<string, List<KeyValuePair<string, int>>> blocksMarketSize = new Dictionary<string, List<KeyValuePair<string, int>>>();
            foreach (var block in blocks)
            {
                var places = _context.Places.Where(x => x.BlockId == block.Id).ToList();
                List<KeyValuePair<string, KeyValuePair<string, int>>> placesData = new List<KeyValuePair<string, KeyValuePair<string, int>>>();
                foreach (var place in places)
                {
                    var data = _context.PinSections.Where(x => x.PlaceId == place.Id)
                         .GroupBy(x => x.BrandId,
                         (key, g) => new KeyValuePair<string, KeyValuePair<string, int>>(place.Name,
                         new KeyValuePair<string, int>(GetBrandName(brands, key), g.Count())))
                         .ToList();
                    placesData.AddRange(data);
                }
                var blockMarketSize = placesData.GroupBy(x => x.Value.Key,
                    (key, g) => new KeyValuePair<string, int>(key, g.Select(x => x.Value.Value).Sum())).ToList();
                if (blocksMarketSize.ContainsKey(block.Name))
                {
                    blocksMarketSize.Add($"{block.Name}-{new Random().Next(10)}", blockMarketSize);
                }
                else
                {
                    blocksMarketSize.Add(block.Name, blockMarketSize);
                }
            }
            return blocksMarketSize;
        }

        string GetBrandName(List<Brand> brands, int? id)
        {
            return brands.FirstOrDefault(c => c.Id == id) != null ? brands.FirstOrDefault(c => c.Id == id).Name : "";
        }
    }
}