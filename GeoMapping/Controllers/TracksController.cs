﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GeoMapping.Data;
using GeoMapping.Models;
using GeoMapping.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using GeoMapping.Controllers.Helpers;

namespace GeoMapping.Controllers
{
    //[Authorize]
    public class TracksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TracksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Tracks
        public IActionResult Index()
        {
            List<TrackingViewModel> trackingVMList = new List<TrackingViewModel>();
            var tracks = _context.Tracks.Include(t => t.Agent).ToList().GroupBy(x => x.AgentId).ToList();
            for (int i = 0; i < tracks.Count; i++)
            {
                var locations = tracks[i].ToList();

                TrackingViewModel temp = new TrackingViewModel()
                {
                    AgentId = locations[i].AgentId,
                    AgentName = locations[i].Agent.UserName,
                    Tracks = new List<PointLocations>()
                };
                for (int j = 0; j < locations.Count; j++)
                {
                    PointLocations pointLocation = new PointLocations()
                    {
                        Location = new Point()
                        {
                            Lat = locations[j].Lat,
                            Lng = locations[j].Long
                        },
                        CreatedDate = locations[j].CreatedDate
                    };
                    temp.Tracks.Add(pointLocation);
                }
                trackingVMList.Add(temp);
            }
            var agents = Help.GetAgents(_context);
            ViewData["AgentId"] = new SelectList(agents, "Id", "UserName");
            return View(trackingVMList);
        }

        public List<TrackingViewModel> GetAllByAgentId(string agentId)
        {
            List<TrackingViewModel> trackingVMList = new List<TrackingViewModel>();
            var tracks = _context.Tracks.Include(t => t.Agent).Where(t => t.AgentId.Equals(agentId)).ToList().GroupBy(x => x.AgentId).ToList();
            for (int i = 0; i < tracks.Count; i++)
            {
                var locations = tracks[i].ToList();

                TrackingViewModel temp = new TrackingViewModel()
                {
                    AgentId = locations[i].AgentId,
                    AgentName = locations[i].Agent.UserName,
                    Tracks = new List<PointLocations>()
                };
                for (int j = 0; j < locations.Count; j++)
                {
                    PointLocations pointLocation = new PointLocations()
                    {
                        Location = new Point()
                        {
                            Lat = locations[j].Lat,
                            Lng = locations[j].Long
                        },
                        CreatedDate = locations[j].CreatedDate
                    };
                    temp.Tracks.Add(pointLocation);
                }
                trackingVMList.Add(temp);
            }
            return trackingVMList;
        }

        public List<TrackingViewModel> GetLatestByAgentId(string agentId)
        {
            List<TrackingViewModel> trackingVMList = new List<TrackingViewModel>();
            var track = _context.Tracks.Include(t => t.Agent)
                                        .Where(t => t.AgentId.Equals(agentId))
                                        .OrderByDescending(t => t.CreatedDate)
                                        .Take(1).FirstOrDefault();
            if (track != null)
            {
                TrackingViewModel temp = new TrackingViewModel()
                {
                    AgentId = track.AgentId,
                    AgentName = track.Agent.UserName,
                    Tracks = new List<PointLocations>()
                };
                PointLocations pointLocation = new PointLocations()
                {
                    Location = new Point()
                    {
                        Lat = track.Lat,
                        Lng = track.Long
                    },
                    CreatedDate = track.CreatedDate
                };
                temp.Tracks.Add(pointLocation);
                trackingVMList.Add(temp);
            }
            return trackingVMList;
        }

        // GET: Tracks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var track = await _context.Tracks
                .Include(t => t.Agent)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (track == null)
            {
                return NotFound();
            }

            return View(track);
        }

        // GET: Tracks/Create
        public IActionResult Create()
        {
            ViewData["AgentId"] = new SelectList(_context.ApplicationUser, "Id", "Id");
            return View();
        }

        // POST: Tracks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CreatedDate,Lat,Long,AgentId")] Track track)
        {
            if (ModelState.IsValid)
            {
                _context.Add(track);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AgentId"] = new SelectList(_context.ApplicationUser, "Id", "Id", track.AgentId);
            return View(track);
        }

        // GET: Tracks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var track = await _context.Tracks.SingleOrDefaultAsync(m => m.Id == id);
            if (track == null)
            {
                return NotFound();
            }
            ViewData["AgentId"] = new SelectList(_context.ApplicationUser, "Id", "Id", track.AgentId);
            return View(track);
        }

        // POST: Tracks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CreatedDate,Lat,Long,AgentId")] Track track)
        {
            if (id != track.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(track);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TrackExists(track.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AgentId"] = new SelectList(_context.ApplicationUser, "Id", "Id", track.AgentId);
            return View(track);
        }

        // GET: Tracks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var track = await _context.Tracks
                .Include(t => t.Agent)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (track == null)
            {
                return NotFound();
            }

            return View(track);
        }

        // POST: Tracks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var track = await _context.Tracks.SingleOrDefaultAsync(m => m.Id == id);
            _context.Tracks.Remove(track);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TrackExists(int id)
        {
            return _context.Tracks.Any(e => e.Id == id);
        }
    }
}
