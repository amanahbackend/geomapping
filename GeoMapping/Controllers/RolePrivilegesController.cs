﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GeoMapping.Data;
using GeoMapping.Models;
using GeoMapping.Controllers.Helpers;
using Microsoft.AspNetCore.Identity;

namespace GeoMapping.Controllers
{
    public class RolePrivilegesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;

        public RolePrivilegesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _context = context;
        }

        // GET: RolePrivileges
        public async Task<IActionResult> Index()
        {
            return View(await _context.RolePrivileges.ToListAsync());
        }

        // GET: RolePrivileges/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rolePrivilege = await _context.RolePrivileges
                .SingleOrDefaultAsync(m => m.Id == id);
            if (rolePrivilege == null)
            {
                return NotFound();
            }

            return View(rolePrivilege);
        }

        // GET: RolePrivileges/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: RolePrivileges/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Fk_ApplicationRole_Id,Fk_Privilege_Id")] RolePrivilege rolePrivilege)
        {
            if (ModelState.IsValid)
            {
                _context.Add(rolePrivilege);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(rolePrivilege);
        }

        // GET: RolePrivileges/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rolePrivilege = await _context.RolePrivileges.SingleOrDefaultAsync(m => m.Id == id);
            if (rolePrivilege == null)
            {
                return NotFound();
            }
            return View(rolePrivilege);
        }

        // POST: RolePrivileges/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Fk_ApplicationRole_Id,Fk_Privilege_Id")] RolePrivilege rolePrivilege)
        {
            if (id != rolePrivilege.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(rolePrivilege);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RolePrivilegeExists(rolePrivilege.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(rolePrivilege);
        }

        // GET: RolePrivileges/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rolePrivilege = await _context.RolePrivileges
                .SingleOrDefaultAsync(m => m.Id == id);
            if (rolePrivilege == null)
            {
                return NotFound();
            }

            return View(rolePrivilege);
        }

        // POST: RolePrivileges/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var rolePrivilege = await _context.RolePrivileges.SingleOrDefaultAsync(m => m.Id == id);
            _context.RolePrivileges.Remove(rolePrivilege);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RolePrivilegeExists(int id)
        {
            return _context.RolePrivileges.Any(e => e.Id == id);
        }

        [HttpGet]
        public List<Privilege> GetByRoleId(string roleId)
        {
            var privIds = _context.RolePrivileges.Where(x => x.Fk_ApplicationRole_Id.Equals(roleId)).Select(x => x.Fk_Privilege_Id).ToList();
            var privs = _context.Privileges.Where(x => privIds.Contains(x.Id)).ToList();
            return privs;
        }
        [HttpGet]
        public async Task<IActionResult> GetPriveleges()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            string userId = user.Id;
            var rolesIds = _context.UserRoles.Where(x => x.UserId.Equals(userId)).Select(x => x.RoleId).ToList();
            List<string> result = new List<string>();
            foreach (var item in rolesIds)
            {
                result.AddRange(GetByRoleId(item).Select(x => x.Name).ToList());
            }
            return Ok(result);
        }

        [HttpPost]
        public bool AddOrUpdate(string roleId, List<int> privIds)
        {
            var dbPrivs = _context.RolePrivileges.Where(x => x.Fk_ApplicationRole_Id == roleId).ToList();
            _context.RolePrivileges.RemoveRange(dbPrivs);
            _context.SaveChanges();
            List<RolePrivilege> rolePrivileges = new List<RolePrivilege>();
            foreach (var item in privIds)
            {
                RolePrivilege rolePrivilege = new RolePrivilege
                {
                    Fk_ApplicationRole_Id = roleId,
                    Fk_Privilege_Id = item
                };
                rolePrivileges.Add(rolePrivilege);
            }
            _context.RolePrivileges.AddRange(rolePrivileges);
            return _context.SaveChanges() > 0;
        }

    }
}
