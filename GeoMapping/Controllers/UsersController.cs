﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GeoMapping.Data;
using GeoMapping.Models;
using GeoMapping.Models.ViewModels;
using Microsoft.AspNetCore.Identity;
using GeoMapping.Models.AccountViewModels;

namespace GeoMapping.Controllers
{
    public class UsersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public UsersController(ApplicationDbContext context, RoleManager<IdentityRole> roleManager,
            UserManager<ApplicationUser> userManager)
        {
            _roleManager = roleManager;
            _context = context;
            _userManager = userManager;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            var users = _context.ApplicationUser.ToList();
            List<UserViewModel> userList = new List<UserViewModel>();
            for (int i = 0; i < users.Count; i++)
            {
                var roleId = _context.UserRoles.Where(x => x.UserId == users[i].Id).FirstOrDefault().RoleId;
                var roles = _context.Roles.ToList();
                var role = _context.Roles.Where(x => x.Id == roleId).FirstOrDefault().Name;
                UserViewModel userView = new UserViewModel()
                {
                    Id = users[i].Id,
                    Email = users[i].Email,
                    PhoneNumber = users[i].PhoneNumber,
                    RoleId = roleId,
                    RoleName = role,
                    UserName = users[i].UserName
                };
                userList.Add(userView);
            }

            return View(userList);
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.ApplicationUser
                .SingleOrDefaultAsync(m => m.Id == id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return View(applicationUser);
        }

        // GET: Users/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RoleId,Id,UserName,NormalizedUserName,Email,NormalizedEmail,EmailConfirmed,PasswordHash,SecurityStamp,ConcurrencyStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEnd,LockoutEnabled,AccessFailedCount")] ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                _context.Add(applicationUser);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(applicationUser);
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.ApplicationUser.SingleOrDefaultAsync(m => m.Id == id);
            if (applicationUser == null)
            {
                return NotFound();
            }
            var userRole = _context.UserRoles.FirstOrDefault(x => x.UserId == id);

            var regsiterModel = new RegisterViewModel
            {
                Email = applicationUser.Email,
                PhoneNumber = applicationUser.PhoneNumber,
                UserName = applicationUser.UserName,
                RoleId = userRole.RoleId,
                Id = id
            };
            var Roles = _roleManager.Roles.Select(x => new { Id = x.Id, Value = x.Name }).ToList();
            ViewBag.Roles = new SelectList(Roles, "Id", "Value");
            return View(regsiterModel);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("RoleId,Id,UserName,Email,PhoneNumber")] RegisterViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }
            var applicationUser = _context.Users.FirstOrDefault(x => x.Id == model.Id);
            applicationUser.UserName = model.UserName;
            applicationUser.Email = model.Email;
            applicationUser.PhoneNumber = model.PhoneNumber;
            
                try
                {
                    _context.Update(applicationUser);
                    await _context.SaveChangesAsync();
                    var roleIds = _context.UserRoles.Where(x => x.UserId == model.Id).Select(x => x.RoleId).ToList();
                    var roles = _context.Roles.Where(x => roleIds.Contains(x.Id)).Select(x => x.Name).ToList();
                    await _userManager.RemoveFromRolesAsync(applicationUser, roles);
                    var role = _context.Roles.FirstOrDefault(x => x.Id == model.RoleId);
                    _userManager.AddToRoleAsync(applicationUser, role.Name);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicationUserExists(applicationUser.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            
            //return View(model);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.ApplicationUser
                .SingleOrDefaultAsync(m => m.Id == id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return View(applicationUser);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var applicationUser = await _context.ApplicationUser.SingleOrDefaultAsync(m => m.Id == id);
            _context.ApplicationUser.Remove(applicationUser);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ApplicationUserExists(string id)
        {
            return _context.ApplicationUser.Any(e => e.Id == id);
        }

        [HttpGet]
        public List<ApplicationUser> GetAll()
        {
            return _context.Users.ToList();
        }
    }
}
