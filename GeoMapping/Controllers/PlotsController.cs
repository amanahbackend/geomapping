﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GeoMapping.Data;
using GeoMapping.Models;
using GeoMapping.Models.ViewModels;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using GeoMapping.Controllers.Helpers;
using Microsoft.AspNetCore.Http;
using System.IO;
using GeoMapping.ExcelHelper;

namespace GeoMapping.Controllers
{
    [Authorize]
    [Route("Plots")]
    public class PlotsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlotsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Plots
        [HttpGet]
        [Route("Index")]
        public IActionResult Index()
        {
            var plotsVM = new List<PlotViewModel>();
            var plots = _context.Plots.Include(p => p.Block).Include(p => p.PlotType).ToList();
            for (int i = 0; i < plots.Count; i++)
            {
                if (plots[i].Polygon != "Random")
                {

                    PlotViewModel plotViewModel = new PlotViewModel()
                    {
                        Id = plots[i].Id,
                        Name = plots[i].Name,
                        BlockName = plots[i].Block?.Name,
                        //PaciNo = plots[i].PACI_NO,
                        PahwNo = plots[i].PAHW_NO,
                        Polygon = JsonConvert.DeserializeObject<List<Point>>(plots[i].Polygon),
                        LicenseNumber = plots[i].LicenseNumber,
                        LicenseType = plots[i].LicenseType,
                        OwnerName = plots[i].OwnerName,
                        PlotTypeName = plots[i].PlotType?.Name,
                        PlotKey = plots[i].PlotKey,
                        Address = plots[i].Address
                    };
                    plotsVM.Add(plotViewModel);
                }
            }
            //var applicationDbContext = _context.Plots.Include(p => p.Area);
            return View(plotsVM);
        }

        [HttpGet]
        [Route("GetAll")]
        public List<PlotViewModel> GetAll()
        {
            var plotsVM = new List<PlotViewModel>();
            var plots = _context.Plots.Include(p => p.Block).Include(p => p.PlotType).ToList();
            for (int i = 0; i < plots.Count; i++)
            {
                PlotViewModel plotViewModel = new PlotViewModel()
                {
                    Id = plots[i].Id,
                    Name = plots[i].Name,
                    BlockName = plots[i].Block.Name,
                    //PaciNo = plots[i].PACI_NO,
                    PahwNo = plots[i].PAHW_NO,
                    Polygon = JsonConvert.DeserializeObject<List<Point>>(plots[i].Polygon),
                    LicenseNumber = plots[i].LicenseNumber,
                    LicenseType = plots[i].LicenseType,
                    OwnerName = plots[i].OwnerName,
                    PlotTypeName = plots[i].PlotType.Name
                };
                plotsVM.Add(plotViewModel);
            }
            return plotsVM;
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public PlotViewModel GetById(int id)
        {
            var plot = _context.Plots.Where(x => x.Id == id).Include(p => p.Block).Include(p => p.PlotType).FirstOrDefault();
            PlotViewModel plotViewModel = new PlotViewModel()
            {
                Id = plot.Id,
                Name = plot.Name,
                BlockName = plot.Block.Name,
                //PaciNo = plot.PACI_NO,
                PahwNo = plot.PAHW_NO,
                Polygon = JsonConvert.DeserializeObject<List<Point>>(plot.Polygon),
                LicenseNumber = plot.LicenseNumber,
                LicenseType = plot.LicenseType,
                OwnerName = plot.OwnerName,
                PlotTypeName = plot.PlotType.Name
            };
            return plotViewModel;
        }

        [HttpGet]
        [Route("GetByBlockId/{id}")]
        public List<PlotViewModel> GetByBlockId(int id)
        {
            var plotsVM = new List<PlotViewModel>();
            var plots = _context.Plots.Where(x => x.BlockId == id).Include(p => p.Block).Include(p => p.PlotType).ToList();
            for (int i = 0; i < plots.Count; i++)
            {
                PlotViewModel plotViewModel = new PlotViewModel()
                {
                    Id = plots[i].Id,
                    Name = plots[i].Name,
                    BlockName = plots[i].Block.Name,
                    //PaciNo = plots[i].PACI_NO,
                    PahwNo = plots[i].PAHW_NO,
                    Polygon = JsonConvert.DeserializeObject<List<Point>>(plots[i].Polygon),
                    LicenseNumber = plots[i].LicenseNumber,
                    LicenseType = plots[i].LicenseType,
                    OwnerName = plots[i].OwnerName,
                    PlotTypeName = plots[i].PlotType.Name
                };
                plotsVM.Add(plotViewModel);
            }
            return plotsVM;
        }

        // GET: Plots/Details/5
        [HttpGet]
        [Route("Details/{id}")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plot = await _context.Plots
                .Include(p => p.Block)
                .Include(p => p.PlotType)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (plot == null)
            {
                return NotFound();
            }

            return View(plot);
        }

        // GET: Plots/Create
        [HttpGet]
        [Route("Create")]
        public IActionResult Create()
        {
            ViewData["BlockId"] = new SelectList(_context.Blocks.Where(x => x.Id != 1), "Id", "Name");
            ViewData["PlotTypeId"] = new SelectList(_context.PlotTypes, "Id", "Name");

            return View();
        }

        // POST: Plots/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Polygon,BlockId,PlotTypeId")] Plot plot)
        {
            if (ModelState.IsValid)
            {
                _context.Add(plot);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BlockId"] = new SelectList(_context.Blocks, "Id", "Id", plot.BlockId);
            ViewData["PlotTypeId"] = new SelectList(_context.PlotTypes, "Id", "Name", plot.PlotTypeId);

            return View(plot);
        }


        [HttpPost]
        [Route("AddPolygon")]
        public bool AddPolygon(string name, string polygon, int blockId, string pahwNo,
            int plotTypeId, string address, string licenseNumber, string licenseType, string ownerName)
        {
            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(polygon))
            {
                Plot area = new Plot()
                {
                    Name = name,
                    BlockId = blockId,
                    Polygon = polygon,
                    //PACI_NO = paciNo,
                    PAHW_NO = pahwNo,
                    PlotTypeId = plotTypeId,
                    Address = address,
                    LicenseNumber = licenseNumber,
                    LicenseType = licenseType,
                    OwnerName = ownerName
                };
                _context.Add(area);
                _context.SaveChanges();
                //return RedirectToAction(nameof(Index));
                return true;
            }
            return false;
        }

        // GET: Plots/Edit/5
        [HttpGet]
        [Route("Edit/{id}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plot = await _context.Plots.SingleOrDefaultAsync(m => m.Id == id);
            if (plot == null)
            {
                return NotFound();
            }
            ViewData["BlockId"] = new SelectList(_context.Blocks, "Id", "Id", plot.BlockId);
            ViewData["PlotTypeId"] = new SelectList(_context.PlotTypes, "Id", "Name", plot.PlotTypeId);

            return View(plot);
        }

        // POST: Plots/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Edit")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Polygon,BlockId,PlotTypeId")] Plot plot)
        {
            if (id != plot.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plot);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlotExists(plot.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BlockId"] = new SelectList(_context.Blocks, "Id", "Id", plot.BlockId);
            ViewData["PlotTypeId"] = new SelectList(_context.PlotTypes, "Id", "Name", plot.PlotTypeId);

            return View(plot);
        }

        // GET: Plots/Delete/5
        [HttpGet]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plot = await _context.Plots
                .Include(p => p.Block)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (plot == null)
            {
                return NotFound();
            }

            return View(plot);
        }

        // POST: Plots/Delete/5
        [HttpPost, ActionName("Delete"), Route("Delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plot = await _context.Plots.SingleOrDefaultAsync(m => m.Id == id);
            _context.Plots.Remove(plot);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlotExists(int id)
        {
            return _context.Plots.Any(e => e.Id == id);
        }


        // GET: Plots/Create
        [HttpGet]
        [Route("AssignToAgent")]
        public IActionResult AssignToAgent()
        {
            var agents = Help.GetAgents(_context);
            ViewData["AgentId"] = new SelectList(agents, "Id", "UserName");
            ViewData["PlotId"] = new SelectList(_context.Plots.Where(x => string.IsNullOrEmpty(x.AssignedAgentId) == true), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("AssignToAgent")]
        public async Task<IActionResult> AssignToAgent(AssignPlotViewModel assign)
        {
            if (ModelState.IsValid)
            {
                var plot = _context.Plots.Where(x => x.Id == assign.PlotId).FirstOrDefault();
                plot.AssignedAgentId = assign.AgentId;
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            var agents = Help.GetAgents(_context);
            ViewData["AgentId"] = new SelectList(agents, "Id", "UserName");
            ViewData["PlotId"] = new SelectList(_context.Plots.Where(x => string.IsNullOrEmpty(x.AssignedAgentId) == true), "Id", "Name");
            return View(assign);
        }

        [HttpGet]
        [Route("GetPlotTypes")]
        public List<PlotType> GetPlotTypes()
        {
            return _context.PlotTypes.ToList();
        }

        [HttpGet("AddFromFile")]
        public IActionResult AddFromFile()
        {
            return View();
        }

        [HttpPost]
        [Route("AddFromFile")]
        public async Task<IActionResult> AddFromFile(IFormFile file)
        {
            var filePath = AppDomain.CurrentDomain.BaseDirectory + file.FileName;
            if (file.Length > 0)
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }
            IList<Plot> dataList = ExcelReader.GetDataToList(filePath, AddPlotData);
            AddOrUpdatePlots(dataList.ToList());
            return RedirectToAction(nameof(Index));
        }

        public void AddOrUpdatePlots(List<Plot> plots)
        {
            foreach (var item in plots)
            {
                var plot = _context.Plots.FirstOrDefault(p => p.PlotKey == item.PlotKey);
                if (plot != null)
                {
                    plot.Address = item.Address;
                    plot.LicenseNumber = item.LicenseNumber;
                    plot.LicenseType = item.LicenseType;
                    plot.Name = item.Name;
                    plot.OwnerName = item.OwnerName;
                    plot.PAHW_NO = item.PAHW_NO;
                    plot.Polygon = item.Polygon;
                    _context.SaveChanges();
                }
                else
                {
                    _context.Plots.Add(item);
                }
            }
            _context.SaveChanges();
        }


        public static Plot AddPlotData(IList<string> rowData, IList<string> columnNames)
        {
            var plot = new Plot()
            {
                Address = rowData[columnNames.IndexFor("Address")],
                LicenseNumber = rowData[columnNames.IndexFor("LicenseNumber")],
                LicenseType = rowData[columnNames.IndexFor("LicenseType")],
                Name = rowData[columnNames.IndexFor("Name")],
                PAHW_NO = rowData[columnNames.IndexFor("PAHW_NO")],
                OwnerName = rowData[columnNames.IndexFor("OwnerName")],
                PlotKey = rowData[columnNames.IndexFor("PlotKey")].ToInt32Nullable(),
                Polygon = rowData[columnNames.IndexFor("Polygon")]
            };
            return plot;
        }

        [HttpGet]
        [Route("GetCount")]
        public int GetCount()
        {
            return _context.Plots.Count();
        }
    }
}
