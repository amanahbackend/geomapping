﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeoMapping.Data;
using GeoMapping.Models;
using GeoMapping.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GeoMapping.Controllers
{
    [Authorize]
    public class SurveysController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SurveysController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public List<SurveryViewModel> Filter([FromBody]List<ReportFilter> filters)
        {
            var predicate = PredicateBuilder.True<SurveyHistory>();
            foreach (var item in filters)
            {
                if (item.Type.ToLower().Contains("date"))
                {
                    predicate = predicate.And(PredicateBuilder.CreateGreaterThanOrLessThanSingleExpression<SurveyHistory>(item.Type, item.Value));
                }
                else
                {
                    predicate = predicate.And(PredicateBuilder.CreateEqualSingleExpression<SurveyHistory>(item.Type, item.Value));
                }
            }

            var surveys = _context.SurveyHistories
                                    .Include(p => p.Place)
                                    .Include(p => p.Status)
                                    .Include(p => p.CreatedUser)
                                    .Where(predicate)
                                    .Select(p => new SurveryViewModel
                                    {
                                        Id = p.Id,
                                        StatusName = p.Status != null ? p.Status.Name : "",
                                        CreatedDate = p.CreatedDate.ToString("yyyy-MM-dd"),
                                        PlaceName = p.Place != null ? p.Place.Name : "",
                                        UserName = p.CreatedUser != null ? p.CreatedUser.UserName : ""
                                    }).ToList();

            return surveys;
        }

    }
}