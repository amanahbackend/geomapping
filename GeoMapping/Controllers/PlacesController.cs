﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GeoMapping.Data;
using GeoMapping.Models;
using GeoMapping.Models.ViewModels;
using Newtonsoft.Json;
using GeoMapping.Controllers.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using GeoMapping.PACI;

namespace GeoMapping.Controllers
{
    //[Authorize]
    [Route("Places")]
    public class PlacesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfigurationRoot _configurationRoot;
        public PlacesController(ApplicationDbContext context, IConfigurationRoot configurationRoot)
        {
            _configurationRoot = configurationRoot;
            _context = context;
        }

        // GET: Places
        [Route("Index")]
        public IActionResult Index()
        {
            var placesVM = new List<PlaceViewModel>();
            var places = _context.Places.Include(p => p.Area).Include(p => p.Block).Include(p => p.Plot).ToList();
            for (int i = 0; i < places.Count; i++)
            {
                PlaceViewModel placeViewModel = new PlaceViewModel()
                {
                    Id = places[i].Id,
                    Name = places[i].Name,
                    AreaName = places[i].Area.Name,
                    PlotName = places[i].Plot != null ? places[i].Plot.Name : "",
                    BlockName = places[i].Block?.Name,
                    PACINo = places[i].PACINo,
                    Points = JsonConvert.DeserializeObject<Point>(places[i].Point)
                };
                placesVM.Add(placeViewModel);
            }
            //var applicationDbContext = _context.Plots.Include(p => p.Area);
            return View(placesVM);
        }

        [HttpPost]
        [Route("AddPlaces")]
        public bool AddPlaces(string name, string places, int areaId, int plotId,
            int blockId, string paciNo, string streetName, int segmentId, int subsegmentId)
        {
            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(places))
            {
                var points = JsonConvert.DeserializeObject<List<Point>>(places);
                List<Place> placesList = new List<Place>(); ;
                for (int i = 0; i < points.Count; i++)
                {
                    var point = Help.ToPointDbStructure(points[i]);
                    Place place = new Place()
                    {
                        Name = name,
                        AreaId = areaId,
                        Point = point,
                        PlotId = plotId,
                        BlockId = blockId,
                        PACINo = paciNo,
                        StreetName = streetName,
                        SegmentId = segmentId,
                        SubSegmentId = subsegmentId
                    };
                    placesList.Add(place);
                }
                _context.AddRange(placesList);
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        [HttpPost]
        [Route("AddDtailedPlace")]
        public bool AddDtailedPlace(string areaNo, string govObjectId, string areaPolygon, string areaName,
            string blockName, string paciBlockId, string blockPolygon,
            string name, string placePoint, string paciNo, string streetName, int segmentId, int subsegmentId)
        {
            bool result = false;
            var pointObj = JsonConvert.DeserializeObject<Point>(placePoint);
            var point = Help.ToPointDbStructure(pointObj);

            Place place = new Place()
            {
                Name = name,
                Point = point,
                PACINo = paciNo,
                StreetName = streetName,
                SegmentId = segmentId,
                SubSegmentId = subsegmentId
            };

            var area = _context.Areas.FirstOrDefault(a => a.AreaNo.Equals(areaNo));
            int areaId;
            if (area != null)
            {
                place.AreaId = areaId = area.Id;
            }
            else
            {
                if (String.IsNullOrEmpty(areaPolygon))
                {
                    string proxyUrl = _configurationRoot["Location:ProxyUrl"];
                    string serviceUrl = _configurationRoot["Location:AreaService:ServiceUrl"];
                    string areaNofieldName = _configurationRoot["Location:AreaService:AreaNoFieldName"];
                    var areaPaci = PACIHelper.GetArea(Convert.ToInt32(areaNo), areaNofieldName, proxyUrl, serviceUrl);
                    areaPolygon = Help.ToPolygonDbStructure(areaPaci.features[0].geometry.rings);
                }
                var newArea = new Area()
                {
                    Name = areaName,
                    Polygon = areaPolygon,
                    GovObjectId = govObjectId,
                    AreaNo = areaNo
                };
                _context.Add(newArea);
                _context.SaveChanges();
                place.AreaId = areaId = newArea.Id;
            }

            var block = _context.Blocks.FirstOrDefault(b => b.PACIBlockId.Equals(paciBlockId));
            if (block != null)
            {
                place.BlockId = block.Id;
            }
            else
            {
                if (String.IsNullOrEmpty(blockPolygon))
                {
                    string proxyUrl = _configurationRoot["Location:ProxyUrl"];
                    string serviceUrl = _configurationRoot["Location:BlockService:ServiceUrl"];
                    string blockIdfieldName = _configurationRoot["Location:BlockService:BlockIdFieldName"];
                    var blockPaci = PACIHelper.GetBlock(paciBlockId, proxyUrl, serviceUrl, blockIdfieldName);
                    blockPolygon = Help.ToPolygonDbStructure(blockPaci.features[0].geometry.rings);
                }
                Block newBlock = new Block()
                {
                    Name = name,
                    Polygon = blockPolygon,
                    AreaId = Convert.ToInt32(areaId),
                    PACIBlockId = paciBlockId
                };
                _context.Blocks.Add(newBlock);
                _context.SaveChanges();
                place.BlockId = newBlock.Id;
            }
            _context.Places.Add(place);
            result = _context.SaveChanges() > 0;
            return result;
        }
        // GET: Places/Details/5
        [Route("Details")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var place = await _context.Places
                .Include(p => p.Area)
                .Include(p => p.Plot)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (place == null)
            {
                return NotFound();
            }

            return View(place);
        }

        // GET: Places/Create
        [Route("Create")]
        public IActionResult Create()
        {
            ViewData["AreaId"] = new SelectList(_context.Areas.Where(x => x.Id != 1), "Id", "Name");
            ViewData["PlotId"] = new SelectList(_context.Plots.Where(x => x.BlockId == _context.Blocks.Where(a => a.Id != 1).FirstOrDefault().Id), "Id", "Name");
            ViewData["Segment"] = new SelectList(_context.Segments, "Id", "Name");
            ViewData["SubSegment"] = new SelectList(_context.SubSegments, "Id", "Name");
            return View();
        }

        [Route("CreateFromPACI")]
        public IActionResult CreateFromPACI()
        {
            ViewData["Segment"] = new SelectList(_context.Segments, "Id", "Name");
            ViewData["SubSegment"] = new SelectList(_context.SubSegments, "Id", "Name");
            return View();
        }

        // POST: Places/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Create")]
        public async Task<IActionResult> Create([Bind("Id,Name,Point,AreaId,PlotId")] Place place)
        {
            if (ModelState.IsValid)
            {
                _context.Add(place);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AreaId"] = new SelectList(_context.Areas, "Id", "Id", place.AreaId);
            ViewData["PlotId"] = new SelectList(_context.Plots, "Id", "Id", place.PlotId);
            return View(place);
        }

        // GET: Places/Edit/5
        [Route("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var place = await _context.Places.SingleOrDefaultAsync(m => m.Id == id);
            if (place == null)
            {
                return NotFound();
            }
            ViewData["AreaId"] = new SelectList(_context.Areas, "Id", "Id", place.AreaId);
            ViewData["PlotId"] = new SelectList(_context.Plots, "Id", "Id", place.PlotId);
            return View(place);
        }

        // POST: Places/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Edit")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Point,AreaId,PlotId")] Place place)
        {
            if (id != place.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(place);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlaceExists(place.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AreaId"] = new SelectList(_context.Areas, "Id", "Id", place.AreaId);
            ViewData["PlotId"] = new SelectList(_context.Plots, "Id", "Id", place.PlotId);
            return View(place);
        }

        // GET: Places/Delete/5
        [Route("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var place = await _context.Places
                .Include(p => p.Area)
                .Include(p => p.Plot)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (place == null)
            {
                return NotFound();
            }

            return View(place);
        }

        // POST: Places/Delete/5
        [HttpPost, ActionName("Delete"), Route("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var place = await _context.Places.SingleOrDefaultAsync(m => m.Id == id);
            _context.Places.Remove(place);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlaceExists(int id)
        {
            return _context.Places.Any(e => e.Id == id);
        }

        [HttpGet]
        [Route("GetCount")]
        public int GetCount()
        {
            return _context.Places.Count();
        }

        [HttpPost]
        [Route("Filter")]
        public List<PlaceViewModel> Filter([FromBody]List<ReportFilter> filters)
        {
            var predicate = PredicateBuilder.True<Place>();
            foreach (var item in filters)
            {
                predicate = predicate.And(PredicateBuilder.CreateEqualSingleExpression<Place>(item.Type, item.Value));
            }
            var places = _context.Places
                                    .Include(p => p.Area)
                                    .Include(p => p.Block)
                                    .Include(p => p.Status)
                                    .Include(p => p.Plot)
                                    .Include(p => p.Segment)
                                    .Include(p => p.SubSegment)
                                    .Where(predicate)
                                    .Select(p => new PlaceViewModel
                                    {
                                        Name = p.Name,
                                        Id = p.Id,
                                        PACINo = p.PACINo,
                                        AreaName = p.Area != null ? p.Area.Name : "",
                                        BlockName = p.Block != null ? p.Block.Name : "",
                                        PlotName = p.Plot != null ? p.Plot.Name : "",
                                        StatusName = p.Status != null ? p.Status.Name : "",
                                        SegmentName = p.Segment != null ? p.Segment.Name : "",
                                        SubsegmentName = p.SubSegment != null ? p.SubSegment.Name : ""
                                    }).ToList();

            return places;
        }

        [HttpGet]
        [Route("GetAll")]
        public List<Place> GetAll()
        {
            return _context.Places.ToList();
        }

        [HttpGet]
        [Route("GetMarketSize")]
        public Dictionary<string, List<KeyValuePair<string, int>>> GetMarketSize()
        {
            var brands = _context.Brands.ToList();
            Dictionary<string, List<KeyValuePair<string, int>>> placesMarketSize = new Dictionary<string, List<KeyValuePair<string, int>>>();

            var places = _context.Places.Where(x => !x.Name.Equals("random", StringComparison.OrdinalIgnoreCase)).ToList();
            List<KeyValuePair<string, KeyValuePair<string, int>>> placesData = new List<KeyValuePair<string, KeyValuePair<string, int>>>();
            foreach (var place in places)
            {
                var data = _context.PinSections.Where(x => x.PlaceId == place.Id)
                     .GroupBy(x => x.BrandId,
                     (key, g) => new KeyValuePair<string, KeyValuePair<string, int>>(place.Name,
                     new KeyValuePair<string, int>(GetBrandName(brands, key), g.Count())))
                     .ToList();
                placesData.AddRange(data);
            }
            var placeMarketSize = placesData.GroupBy(x => x.Value.Key,
                (key, g) => new KeyValuePair<string, int>(key, g.Select(x => x.Value.Value).Sum())).ToList();
            placesMarketSize.Add("total places", placeMarketSize);

            return placesMarketSize;
        }

        string GetBrandName(List<Brand> brands, int? id)
        {
            return brands.FirstOrDefault(c => c.Id == id) != null ? brands.FirstOrDefault(c => c.Id == id).Name : "";
        }
    }

    public class ReportFilter
    {
        public string Type { get; set; }
        public object Value { get; set; }
    }
}
