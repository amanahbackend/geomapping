﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GeoMapping.Data;
using GeoMapping.Models;
using GeoMapping.Models.ViewModels;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace GeoMapping.Controllers
{
    [Authorize]
    [Route("Areas")]
    public class AreasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AreasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Areas
        [HttpGet]
        [Route("Index")]
        public IActionResult Index()
        {
            var areasVM = new List<AreaViewModel>();
            var areas = _context.Areas.ToList();
            for (int i = 0; i < areas.Count; i++)
            {
                if (areas[i].Polygon != "Random")
                {
                    AreaViewModel areaViewModel = new AreaViewModel()
                    {
                        Id = areas[i].Id,
                        Name = areas[i].Name,
                        Polygon = JsonConvert.DeserializeObject<List<Point>>(areas[i].Polygon),
                        GovObjectId = areas[i].GovObjectId,
                        AreaNo = areas[i].AreaNo
                    };
                    areasVM.Add(areaViewModel);
                }
            }
            return View(areasVM);
        }

        [HttpGet]
        [Route("GetAll")]
        public List<AreaViewModel> GetAll()
        {
            var areasVM = new List<AreaViewModel>();
            var areas = _context.Areas.ToList();
            for (int i = 0; i < areas.Count; i++)
            {
                if (areas[i].Polygon != "Random")
                {
                    AreaViewModel areaViewModel = new AreaViewModel()
                    {
                        Id = areas[i].Id,
                        Name = areas[i].Name,
                        Polygon = JsonConvert.DeserializeObject<List<Point>>(areas[i].Polygon),
                        GovObjectId = areas[i].GovObjectId,
                        AreaNo = areas[i].AreaNo
                    };
                    areasVM.Add(areaViewModel);
                }
            }
            return areasVM;
        }

        [HttpGet]
        [Route("GetAreaById/{id}")]
        public AreaViewModel GetAreaById(int id)
        {
            var area = _context.Areas.Where(x => x.Id == id).FirstOrDefault();

            AreaViewModel areaViewModel = new AreaViewModel()
            {
                Id = area.Id,
                Name = area.Name,
                Polygon = JsonConvert.DeserializeObject<List<Point>>(area.Polygon)
            };
            return areaViewModel;
        }

        // GET: Areas/Details/5
        [HttpGet]
        [Route("Details")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var area = await _context.Areas
                .SingleOrDefaultAsync(m => m.Id == id);
            if (area == null)
            {
                return NotFound();
            }

            return View(area);
        }

        // GET: Areas/Create
        [HttpGet]
        [Route("Create")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Areas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Create")]
        public async Task<IActionResult> Create(Area area)
        {
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(area.Polygon))
                {
                    _context.Add(area);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View(area);
                }
            }
            return View(area);
        }
        [HttpPost]
        [Route("AddPolygon")]
        public bool AddPolygon(string name, string areaNo, string polygon, string govObjectId)
        {
            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(polygon))
            {
                Area area = new Area()
                {
                    Name = name,
                    Polygon = polygon,
                    GovObjectId = govObjectId,
                    AreaNo = areaNo
                };
                _context.Add(area);
                _context.SaveChanges();
                //return RedirectToAction(nameof(Index));
                return true;
            }
            return false;
        }
        // GET: Areas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var area = await _context.Areas.SingleOrDefaultAsync(m => m.Id == id);
            if (area == null)
            {
                return NotFound();
            }
            return View(area);
        }

        // POST: Areas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Edit")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] Area area)
        {
            if (id != area.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(area);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AreaExists(area.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(area);
        }

        // GET: Areas/Delete/5
        [HttpGet]
        [Route("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var area = await _context.Areas
                .SingleOrDefaultAsync(m => m.Id == id);
            if (area == null)
            {
                return NotFound();
            }

            return View(area);
        }

        // POST: Areas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var area = await _context.Areas.SingleOrDefaultAsync(m => m.Id == id);
            _context.Areas.Remove(area);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AreaExists(int id)
        {
            return _context.Areas.Any(e => e.Id == id);
        }

        [HttpGet]
        [Route("GetCount")]
        public int GetCount()
        {
            return _context.Areas.Count();
        }

        [HttpGet]
        [Route("GetMarketSize")]
        public Dictionary<string, List<KeyValuePair<string, int>>> GetMarketSize()
        {
            var areas = _context.Areas.Where(z => !z.Name.Equals("random", StringComparison.OrdinalIgnoreCase)).ToList();
            var brands = _context.Brands.ToList();
            Dictionary<string, List<KeyValuePair<string, int>>> areasMarketSize = new Dictionary<string, List<KeyValuePair<string, int>>>();
            foreach (var area in areas)
            {
                var places = _context.Places.Where(x => x.AreaId == area.Id).ToList();
                List<KeyValuePair<string, KeyValuePair<string, int>>> placesData = new List<KeyValuePair<string, KeyValuePair<string, int>>>();
                foreach (var place in places)
                {
                    var data = _context.PinSections.Where(x => x.PlaceId == place.Id)
                         .GroupBy(x => x.BrandId,
                         (key, g) => new KeyValuePair<string, KeyValuePair<string, int>>(place.Name,
                         new KeyValuePair<string, int>(GetBrandName(brands, key), g.Count())))
                         .ToList();
                    placesData.AddRange(data);
                }
                var areaMarketSize = placesData.GroupBy(x => x.Value.Key,
                    (key, g) => new KeyValuePair<string, int>(key, g.Select(x => x.Value.Value).Sum())).ToList();
                if (areasMarketSize.ContainsKey(area.Name))
                {
                    areasMarketSize.Add($"{area.Name}-{new Random().Next(10)}", areaMarketSize);
                }
                else
                {
                    areasMarketSize.Add(area.Name, areaMarketSize);
                }
            }
            return areasMarketSize;
        }

        string GetBrandName(List<Brand> brands, int? id)
        {
            return brands.FirstOrDefault(c => c.Id == id) != null ? brands.FirstOrDefault(c => c.Id == id).Name : "";
        }
    }
}
