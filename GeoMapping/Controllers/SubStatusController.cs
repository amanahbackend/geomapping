﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GeoMapping.Data;
using GeoMapping.Models;
using Microsoft.AspNetCore.Authorization;

namespace GeoMapping.Controllers
{
	[Authorize]
    public class SubStatusController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SubStatusController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SubStatus
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.SubStatuses.Include(s => s.Status);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: SubStatus/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subStatus = await _context.SubStatuses
                .Include(s => s.Status)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (subStatus == null)
            {
                return NotFound();
            }

            return View(subStatus);
        }

        // GET: SubStatus/Create
        public IActionResult Create()
        {
            ViewData["StatusId"] = new SelectList(_context.Statuses, "Id", "Id");
            return View();
        }

        // POST: SubStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,StatusId")] SubStatus subStatus)
        {
            if (ModelState.IsValid)
            {
                _context.Add(subStatus);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["StatusId"] = new SelectList(_context.Statuses, "Id", "Id", subStatus.StatusId);
            return View(subStatus);
        }

        // GET: SubStatus/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subStatus = await _context.SubStatuses.SingleOrDefaultAsync(m => m.Id == id);
            if (subStatus == null)
            {
                return NotFound();
            }
            ViewData["StatusId"] = new SelectList(_context.Statuses, "Id", "Id", subStatus.StatusId);
            return View(subStatus);
        }

        // POST: SubStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,StatusId")] SubStatus subStatus)
        {
            if (id != subStatus.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(subStatus);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SubStatusExists(subStatus.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["StatusId"] = new SelectList(_context.Statuses, "Id", "Id", subStatus.StatusId);
            return View(subStatus);
        }

        // GET: SubStatus/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subStatus = await _context.SubStatuses
                .Include(s => s.Status)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (subStatus == null)
            {
                return NotFound();
            }

            return View(subStatus);
        }

        // POST: SubStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var subStatus = await _context.SubStatuses.SingleOrDefaultAsync(m => m.Id == id);
            _context.SubStatuses.Remove(subStatus);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SubStatusExists(int id)
        {
            return _context.SubStatuses.Any(e => e.Id == id);
        }
    }
}
