﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;

namespace GeoMapping.PACI
{
    public class PACIHelper
    {
        public static PACIModel GetPACIModel(int? PACI, string proxyUrl,
                        string paciServiceUrl, string paciNumberFieldName)
        {
            try
            {
                if (PACI != null)
                {
                    var url = ArcGISRestUtilities.ConstructQueryGetURL(proxyUrl, paciServiceUrl,
                        new Dictionary<string, object>
                        {
                            { paciNumberFieldName, PACI }
                        }, false);
                    string responseFromServer = UrlUtilities.GetServerResponse(url);

                    PACIModel oRootObject = new PACIModel();
                    oRootObject = JsonConvert.DeserializeObject<PACIModel>(responseFromServer);
                    return oRootObject;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static PACIInfo GetLocationByPACI(int? PACI, string proxyUrl,
                        string paciServiceUrl, string paciNumberFieldName,
                        string blockServiceUrl, string blockNameFieldNameBlockService, string nhoodNameFieldName,
                        string streetServiceUrl, string blockNameFieldNameStreetService, string streetNameFieldName)
        {
            try
            {
                if (PACI != null)
                {
                    PACIModel oRootObject = GetPACIModel(PACI, proxyUrl, paciServiceUrl, paciNumberFieldName);
                    var featureAttributes = oRootObject.features[0].attributes;

                    var block = GetBlock(featureAttributes.neighborhoodenglish,
                                            featureAttributes.blockenglish, proxyUrl,
                                            blockServiceUrl, blockNameFieldNameBlockService, nhoodNameFieldName);

                    var street = GetStreet(featureAttributes.streetenglish,
                                            featureAttributes.blockenglish, proxyUrl,
                                            streetServiceUrl, streetNameFieldName, blockNameFieldNameStreetService);

                    PACIInfo info = new PACIInfo()
                    {
                        Governorate = new DropPACI()
                        {
                            Id = featureAttributes.governorateid,
                            Name = featureAttributes.governorateenglish
                        },
                        Area = new DropPACI()
                        {
                            Id = featureAttributes.neighborhoodid,
                            Name = featureAttributes.neighborhoodenglish
                        },
                        Block = block,
                        Street = street
                    };
                    return info;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }

        }

        public static StreetPACIResultModel GetStreetPACItModel(string streetName, string blockName,
            string proxyUrl, string serviceUrl, string streetFieldName, string blockNameFieldName)
        {
            try
            {
                if (streetName != null && blockName != null)
                {
                    var URL = ArcGISRestUtilities.ConstructQueryGetURL(proxyUrl, serviceUrl,
                        new Dictionary<string, object>
                        {
                            {blockNameFieldName, blockName },
                            {streetFieldName, streetName}
                        }, false);
                    string responseFromServer = UrlUtilities.GetServerResponse(URL);
                    return JsonConvert.DeserializeObject<StreetPACIResultModel>(responseFromServer);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;

            }
        }

        public static DropPACI GetStreet(string streetName, string blockName, string proxyUrl,
                        string serviceUrl, string streetFieldName, string blockNameFieldName)
        {
            try
            {
                if (streetName != null && blockName != null)
                {
                    var oStreet = GetStreetPACItModel(streetName, blockName, proxyUrl,
                                                        serviceUrl, streetFieldName, blockNameFieldName);

                    return new DropPACI()
                    {
                        Id = oStreet.features[0].attributes.OBJECTID.ToString(),
                        Name = oStreet.features[0].attributes.StreetEnglish
                    };
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public static DropPACI GetBlock(string nhoodName, string blockName, string proxyUrl,
            string serviceUrl, string blockNameFieldName, string nhoodNameFieldName)
        {
            //nhoodName = nhoodName.Replace("\"", "'");
            //blockName = blockName.Replace("\"", "'");
            try
            {
                if (nhoodName != null && blockName != null)
                {
                    var URL = ArcGISRestUtilities.ConstructQueryGetURL(proxyUrl, serviceUrl,
                        new Dictionary<string, object>
                        {
                            {blockNameFieldName, blockName},
                            {nhoodNameFieldName, nhoodName}
                        }, false);

                    string responseFromServer = UrlUtilities.GetServerResponse(URL);
                    var oBlock = JsonConvert.DeserializeObject<BlockPACIResultModel>(responseFromServer);

                    return new DropPACI()
                    {
                        Id = oBlock.features[0].attributes.BlockID,
                        Name = oBlock.features[0].attributes.BlockEnglish
                    };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static BlockPACIResultModel GetBlock(string blockId, string proxyUrl,
           string serviceUrl, string blockIdFieldName)
        {
            try
            {
                if (blockId != null)
                {
                    var URL = ArcGISRestUtilities.ConstructQueryGetURL(proxyUrl, serviceUrl,
                        new Dictionary<string, object>
                        {
                            {blockIdFieldName, blockId},
                        }, true);

                    string responseFromServer = UrlUtilities.GetServerResponse(URL);
                    var oBlock = JsonConvert.DeserializeObject<BlockPACIResultModel>(responseFromServer);

                    return oBlock;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static AreaPACIResultModel GetArea(int? areaNo, string areaNoFieldName, string proxyUrl, string serviceUrl)
        {
            try
            {
                if (areaNo != null)
                {
                    var url = ArcGISRestUtilities.ConstructQueryGetURL(proxyUrl, serviceUrl,
                         new Dictionary<string, object>
                         {
                            {areaNoFieldName, areaNo }
                         }, true);

                    string responseFromServer = UrlUtilities.GetServerResponse(url);
                    var oAreas = JsonConvert.DeserializeObject<AreaPACIResultModel>(responseFromServer);

                    return oAreas;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public static List<DropPACI> GetAllGovernorates(string proxyUrl, string serviceUrl)
        {
            try
            {
                var URL = ArcGISRestUtilities.ConstructQueryGetURL(proxyUrl, serviceUrl,
                    new Dictionary<string, object>
                    {
                        {"1",1}
                    }, false);
                string responseFromServer = UrlUtilities.GetServerResponse(URL);
                var oGov = JsonConvert.DeserializeObject<GovPACIResultModel>(responseFromServer);
                List<DropPACI> outGovs = new List<DropPACI>();
                for (int i = 0; i < oGov.features.Count; i++)
                {
                    DropPACI temp = new DropPACI()
                    {
                        Id = oGov.features[i].attributes.Gov_no.ToString(),
                        Name = oGov.features[i].attributes.GovernorateEnglish
                    };
                    outGovs.Add(temp);
                }
                return outGovs;
            }
            catch (Exception)
            {
                return null;
            }



        }

        public static List<DropPACI> GetAreas(int? govNo, string govNoFieldName, string proxyUrl, string serviceUrl)
        {
            try
            {
                if (govNo != null)
                {
                    var url = ArcGISRestUtilities.ConstructQueryGetURL(proxyUrl, serviceUrl,
                         new Dictionary<string, object>
                         {
                            {govNoFieldName, govNo }
                         }, false);

                    string responseFromServer = UrlUtilities.GetServerResponse(url);
                    var oAreas = JsonConvert.DeserializeObject<AreaPACIResultModel>(responseFromServer);
                    List<DropPACI> outAreas = new List<DropPACI>();
                    for (int i = 0; i < oAreas.features.Count; i++)
                    {
                        DropPACI temp = new DropPACI()
                        {
                            Id = oAreas.features[i].attributes.Nhood_No.ToString(),
                            Name = oAreas.features[i].attributes.NeighborhoodEnglish
                        };
                        outAreas.Add(temp);
                    }
                    return outAreas;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public static List<DropPACI> GetBlocks(int? nhoodNo, string nhoodNoFieldName, string proxyUrl, string serviceUrl)
        {
            try
            {
                if (nhoodNo != null)
                {
                    var url = ArcGISRestUtilities.ConstructQueryGetURL(proxyUrl, serviceUrl,
                             new Dictionary<string, object>
                             {
                                {nhoodNoFieldName, nhoodNo}
                             }, false);
                    string responseFromServer = UrlUtilities.GetServerResponse(url);
                    var oBlocks = JsonConvert.DeserializeObject<BlockPACIResultModel>(responseFromServer);

                    List<DropPACI> outBlocks = new List<DropPACI>();
                    for (int i = 0; i < oBlocks.features.Count; i++)
                    {
                        DropPACI temp = new DropPACI()
                        {
                            Id = oBlocks.features[i].attributes.BlockID,
                            Name = oBlocks.features[i].attributes.BlockEnglish
                        };
                        outBlocks.Add(temp);
                    }
                    return outBlocks;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<DropPACI> GetStreets(int? govId, string govIdFieldName,
                                int? areaId, string areaIdFieldName,
                                string blockName, string blockNameFieldName,
                                string proxyUrl, string serviceUrl)
        {
            try
            {
                if (govId != null && areaId != null && blockName != null && blockName != "")
                {
                    var url = ArcGISRestUtilities.ConstructQueryGetURL(proxyUrl, serviceUrl,
                             new Dictionary<string, object>()
                             {
                                {govIdFieldName, govId},
                                {areaIdFieldName, areaId},
                                {blockNameFieldName, blockName}
                             }, false);
                    string responseFromServer = UrlUtilities.GetServerResponse(url);
                    var oStreets = JsonConvert.DeserializeObject<StreetPACIResultModel>(responseFromServer);
                    List<DropPACI> outAStreets = new List<DropPACI>();
                    for (int i = 0; i < oStreets.features.Count; i++)
                    {
                        DropPACI temp = new DropPACI()
                        {
                            Id = oStreets.features[i].attributes.OBJECTID.ToString(),
                            Name = oStreets.features[i].attributes.StreetEnglish
                        };
                        outAStreets.Add(temp);
                    }
                    return outAStreets;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }

        }




        public class DropPACI
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }

        public class PACIInfo
        {
            public DropPACI Governorate { get; set; }
            public DropPACI Area { get; set; }
            public DropPACI Block { get; set; }
            public DropPACI Street { get; set; }
        }
    }
}
