﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.PACI
{
    public static class ArcGISRestUtilities
    {
        public static string ConstructQueryGetURL(string proxyUrl, string serviceUrl, Dictionary<string, object> parametersValuesDict,bool returnGeometry)

        {
            string whereClause = ConstructWhereClause(parametersValuesDict);
            var query = String.Format("query?{0}&outSR=4326&returnGeometry={1}&outFields=*&f=pjson", whereClause, returnGeometry);
            var URL = String.Format("{0}?{1}/{2}", proxyUrl, serviceUrl, query);
            return URL;
        }

        private static string ConstructWhereClause(Dictionary<string, object> parametersValuesDict)
        {
            string result = "where=";
            foreach (var key in parametersValuesDict.Keys)
            {
                object value = parametersValuesDict[key];
                if (value is int intValue)
                {
                    if (parametersValuesDict.Keys.First().Equals(key))
                    {
                        result += string.Format("{0}={1}", key, intValue);
                    }
                    else
                    {
                        result += string.Format("&{0}={1}", key, intValue);
                    }
                }
                else
                {
                    if (parametersValuesDict.Keys.First().Equals(key))
                    {
                        result += string.Format("{0}='{1}'", key, value.ToString());
                    }
                    else
                    {
                        result += string.Format("&{0}='{1}'", key, value.ToString());
                    }
                }
            }
            return result;
        }
    }
}
