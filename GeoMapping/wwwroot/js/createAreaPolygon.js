﻿var map;
var polygon;
var areaDrawingManager;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 29, lng: 48 },
        zoom: 10
    });

    areaDrawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.POLYGON,
        drawingControl: false,
        polygonOptions: {
            editable: true
        }
    });
    areaDrawingManager.setMap(map);
    google.maps.event.addListener(areaDrawingManager, 'overlaycomplete', function (event) {
        areaDrawingManager.setDrawingMode(null);
        overlayClickListener(event.overlay);
    });
}

function overlayClickListener(overlay) {
    //google.maps.event.addListener(overlay, "mouseup", function (event) {
    
    var polygons = "";
    //if ($('#vertices').val() == "") {
    polygon = overlay.getPath().getArray();
    console.log(polygon);
    //polygons += "["
    //for (var i = 0; i < polygon.length; i++) {
    //    polygons += '{ "lat": "' + polygon[i].lat() + '", "lng": "' + polygon[i].lng() + '" }';
    //    if (i != polygon.length - 1) {
    //        polygons += ',';
    //    }
    //}
    //polygons += "]";
    polygons = getVerticiesFromPolygon(polygon);
    //} else {
    //	console.log("p2");
    //	console.log(overlay.getPath().getArray());
    //	polygon = overlay.getPath().getArray();
    //	polygons += "["
    //	for (var i = 0; i < polygon.length; i++) {
    //		polygons += '{ "lat": "' + polygon[i].lat() + '", "lng": "' + polygon[i].lng() + '" }';
    //		if (i != polygon.length - 1) {
    //			polygons += ',';
    //		}
    //	}
    //	polygons += "]";
    //	polygons = $('#vertices').val() + "|" + polygons;
    //}
    console.log(polygons);
    $('#vertices').val(polygons);
    //});
}