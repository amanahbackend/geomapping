﻿var map;
var infoWindow;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 29, lng: 48 },
        zoom: 9,
        mapTypeId: 'terrain'
    });

    infoWindow = new google.maps.InfoWindow;

    
    if (areas) {
        for (var i = 0; i < areas.length; i++) {
            var p = new google.maps.Polygon({
                paths: areas[i].polygon,
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 3,
                fillColor: '#FF0000',
                fillOpacity: 0.35,
                name: areas[i].name,
                label: "shsh"
            });
            console.log(areas[i]);
            if (areas[i].areaName) {
                p.areaName = areas[i].areaName;
                p.paciNo = areas[i].PACIBlockId;
            }
            else if (areas[i].areaNo) {
                p.areaNo = areas[i].areaNo;
                p.govObjectId = areas[i].govObjectId;
            }
            p.setMap(map);
            addListenersOnPolygon(p);
        }
    }
}

var addListenersOnPolygon = function (polygon) {

    google.maps.event.addListener(polygon, 'click', function (event) {
        debugger;
        var content = "<p>Name: " + polygon.name + "</p>";
        if (polygon.areaName) {
            content += "<p>Area Name: " + polygon.areaName + "</p>";
        }
        if (polygon.areaNo) {
            content += "<p>Area No. : " + polygon.areaNo + "</p>";
        }
        if (polygon.paciNo) {
            content += "<p>PACI No. : " + polygon.paciNo + "</p>";
        }
        if (polygon.pahwNo) {
            content += "<p>PAHW No. : " + polygon.pahwNo + "</p>";
        }
        if (polygon.govObjectId) {
            content += "<p>Gov. No. : " + polygon.govObjectId + "</p>";
        }
        infoWindow.setContent(content);
        infoWindow.setPosition(event.latLng);
        infoWindow.open(map);
    });
}
var drawnPolygons = [];
function drawPolygons(polygonList) {
    for (var i = 0; i < drawnPolygons.length; i++) {
        drawnPolygons[i].setMap(null);
    }
    for (var i = 0; i < polygonList.length; i++) {
        var p = new google.maps.Polygon({
            paths: polygonList[i].polygon,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 3,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            name: polygonList[i].name,
            label: "shsh"
        });

        p.setMap(map);
        addListenersOnPolygon(p);
        drawnPolygons.push(p);
    }
}