﻿function processPoints(geometry, callback, thisArg) {
    if (geometry instanceof google.maps.LatLng) {
        callback.call(thisArg, geometry);
    }
    else if (geometry instanceof google.maps.Data.Point) {
        callback.call(thisArg, geometry.get());
    }
    else {
        geometry.getArray().forEach(function (g) {
            processPoints(g, callback, thisArg);
        });
    }
}

function getPathsFromGeojsonPolygon(feature) {
    debugger;
    var arr = feature.getGeometry().getArray();
    if (arr.length > 1) {
        var paths = [];
        for (var i = 0; i < arr.length; i++) {
            if (feature.getGeometry().getArray()[i].b[0].b) {
                paths.push(feature.getGeometry().getArray()[i].b[0].b);
            }
            else {
                paths.push(feature.getGeometry().getArray()[i].b);
            }
        }
        return paths;
    }
    else {
        return feature.getGeometry().getArray()[0].b;
    }
}

function getVerticiesFromPolygon(polygon) {
    debugger;
    var verticies = "[";
    for (var i = 0; i < polygon.length; i++) {
        verticies += '{ "lat": "' + polygon[i].lat() + '", "lng": "' + polygon[i].lng() + '" }';
        if (i != polygon.length - 1) {
            verticies += ',';
        }
    }
    verticies += "]";
    return verticies;
}

function getVerticiesFromGeojsonPolygon(polygon) {
    debugger;
    var verticies = "[";
    if (polygon.geometry.type === "Polygon") {
        var cors = polygon.geometry.coordinates;
        for (var i = 0; i < cors.length; i++) {
            verticies += '{ "lat": "' + cors[i][1] + '", "lng": "' + cors[i][0] + '" }';
            if (i != cors.length - 1) {
                verticies += ',';
            }
        }
    }
    else if (polygon.geometry.type === "MultiPolygon") {
        var cors = polygon.geometry.coordinates;
        for (var j = 0; j < cors.length; j++) {
            for (var i = 0; i < cors[j][0].length; i++) {
                verticies += '{ "lat": "' + cors[j][0][i][1] + '", "lng": "' + cors[j][0][i][0] + '" }';                
                if (i != cors[j][0].length - 1) {
                    verticies += ',';
                }
            }
            if (j != cors.length - 1) {
                verticies += ',';
            }
        }
    }
    verticies += "]";
    return verticies;
}