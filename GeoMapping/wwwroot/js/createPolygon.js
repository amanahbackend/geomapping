﻿var areaPolygon;
var map;
var drawnPoints = [];
var bounds;
var drawnPolygon;
var polygonArray;

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center: { lat: 29, lng: 48 },
		zoom: 10
	});

	bounds = new google.maps.LatLngBounds();

	var drawingManager = new google.maps.drawing.DrawingManager({
		drawingMode: google.maps.drawing.OverlayType.POLYGON,
		drawingControl: false,
		polygonOptions: {
			editable: true
		}
	});
	drawingManager.setMap(map);
	google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
		drawingManager.setDrawingMode(null);
		drawnPolygon = event.overlay;
		overlayClickListener(event.overlay);
	});
}

function addToMap(paths) {
	if (areaPolygon) {
		areaPolygon.setMap(null);
		bounds = new google.maps.LatLngBounds();
	}

	var points = [];
	for (var i = 0; i < paths.length; i++) {
		point = new google.maps.LatLng(paths[i].lat, paths[i].lng);
		bounds.extend(point);
		points.push(point);
	}

	areaPolygon = new google.maps.Polygon({
		paths: paths,
		strokeColor: '#27771f',
		strokeOpacity: 0.8,
		strokeWeight: 3,
		fillColor: '#53f442',
		fillOpacity: 0.35,
		label: "shsh"
	});
	areaPolygon.setMap(map);
	map.fitBounds(bounds);
}
var shape;
function checkPolygonIsIn(polygonArray) {
	var isAllow = true;
	for (var i = 0; i < polygonArray.length; i++) {
		if (google.maps.geometry.poly.containsLocation(polygonArray[i], areaPolygon) == false && isAllow == true) {
			isAllow = false;
		}
	}
	if (isAllow == false) {
		drawnPolygon.setEditable(true);
		$("#polyError").css("display", "block");
		return false;
	} else {
		$("#polyError").css("display", "none");
		return true;
	}
}

function overlayClickListener(overlay) {
	google.maps.event.addListener(overlay, "mouseup", function (event) {
		polygonArray = overlay.getPath().getArray();

		var allowNow = checkPolygonIsIn(polygonArray);
		if (allowNow == true) {
			var polygon;
			var polygons = "";
			//if ($('#vertices').val() == "") {
			polygon = overlay.getPath().getArray();
			console.log("p");
			polygons += "["
			for (var i = 0; i < polygon.length; i++) {
				polygons += '{ "lat": "' + polygon[i].lat() + '", "lng": "' + polygon[i].lng() + '" }';
				if (i != polygon.length - 1) {
					polygons += ',';
				}
			}
			polygons += "]";
			//} else {
			//	console.log("p2");
			//	console.log(overlay.getPath().getArray());
			//	polygon = overlay.getPath().getArray();
			//	polygons += "["
			//	for (var i = 0; i < polygon.length; i++) {
			//		polygons += '{ "lat": "' + polygon[i].lat() + '", "lng": "' + polygon[i].lng() + '" }';
			//		if (i != polygon.length - 1) {
			//			polygons += ',';
			//		}
			//	}
			//	polygons += "]";
			//	polygons = $('#vertices').val() + "|" + polygons;
			//}
			$('#vertices').val(polygons);
		}
	});
}