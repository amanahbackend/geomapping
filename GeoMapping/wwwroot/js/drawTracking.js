﻿Number.prototype.padLeft = function (base, chr) {
	var len = (String(base || 10).length - String(this).length) + 1;
	return len > 0 ? new Array(len).join(chr || '0') + this : this;
}

function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		center: { lat: 29, lng: 48 },
		zoom: 9,
		mapTypeId: 'terrain'
	});

	// Create an array of alphabetical characters used to label the markers.
	var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

	// Add some markers to the map.
	// Note: The code uses the JavaScript Array.prototype.map() method to
	// create an array of markers based on a given "locations" array.
	// The map() method here has nothing to do with the Google Maps API.
	var markersLoc = [];
	var infoWin = new google.maps.InfoWindow();
	console.log("l");
	console.log(locations);
	for (var i = 0; i < locations.length; i++) {
		for (var j = 0; j < locations[i].tracks.length; j++) {
			var track = locations[i].tracks[j];
			markersLoc.push({
				location: { lat: parseFloat(track.location.lat), lng: parseFloat(track.location.lng) }, agentName: locations[i].agentName, date: track.createdDate, index: i
			});
		}
	}
	console.log(markersLoc);

	var markers = markersLoc.map(function (mark, i) {
		//console.log(mark);
		var marker = new google.maps.Marker({
			//label: labels[mark.index % labels.length],
			position: mark.location,
			icon:"https://maps.gstatic.com/mapfiles/ms2/micons/red-dot.png"
		});
		google.maps.event.addListener(marker, 'click', function (evt) {
			var d = new Date(mark.date);
			dformat = [	d.getDate().padLeft(),
				(d.getMonth() + 1).padLeft(),
			d.getFullYear()].join('/') +
				' ' +
				[d.getHours().padLeft(),
				d.getMinutes().padLeft(),
				d.getSeconds().padLeft()].join(':');
			var content = "<p>Agent Name: " + mark.agentName + "</p>" +
				"<p>Created Date: " + dformat + "</p>";
			infoWin.setContent(content);
			infoWin.open(map, marker);
		})
		return marker;
	});
	// Add a marker clusterer to manage the markers.
	var markerCluster = new MarkerClusterer(map, markers,
		{ imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
}