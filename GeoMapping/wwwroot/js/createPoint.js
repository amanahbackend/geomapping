﻿var markers = [];
var areaPolygon;
var map;
var bounds;
var pointsArray = [];
var drawingManager;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 29, lng: 48 },
        zoom: 10
    });

    bounds = new google.maps.LatLngBounds();

    drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.MARKER,
        drawingControl: false,
        markerOptions: {
            icon: "https://maps.gstatic.com/mapfiles/ms2/micons/red-dot.png",
            editable: true
        }
    });
    drawingManager.setMap(map);
    google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
        markers.push({
            lat: event.overlay.position.lat(),
            lng: event.overlay.position.lng()
        });
        drawnPolygon = event.overlay;
        pointsArray.push(event.overlay);
        overlayClickListener();
        //drawingManager.setDrawingMode(null);
    });
}

function clearMap() {
    if (areaPolygon) {
        areaPolygon.setMap(null);
    }
    for (var i = 0; i < pointsArray.length; i++) {
        pointsArray[i].setMap(null);
    }
    pointsArray = [];
    $("#polyError").css("display", "none");
}

function addToMap(paths) {
    if (areaPolygon) {
        areaPolygon.setMap(null);
        bounds = new google.maps.LatLngBounds();
    }

    var points = [];
    for (var i = 0; i < paths.length; i++) {
        point = new google.maps.LatLng(paths[i].lat, paths[i].lng);
        bounds.extend(point);
        points.push(point);
    }

    areaPolygon = new google.maps.Polygon({
        paths: paths,
        strokeColor: '#27771f',
        strokeOpacity: 0.8,
        strokeWeight: 3,
        fillColor: '#53f442',
        fillOpacity: 0.35,
        label: "shsh"
    });
    areaPolygon.setMap(map);
    map.fitBounds(bounds);
}

function checkPlacesIsIn(polygonArray) {
    var isAllow = true;
    for (var i = 0; i < polygonArray.length; i++) {
        if (google.maps.geometry.poly.containsLocation(polygonArray[i].position, areaPolygon) == false && isAllow == true) {
            isAllow = false;
        }
    }
    if (isAllow == false) {
        $("#polyError").css("display", "block");
        return false;
    } else {
        $("#polyError").css("display", "none");
        return true;
    }
}

function overlayClickListener() {
    var point;
    var polygons = "";

    var markerAsText = JSON.stringify(markers);
    if (checkPlacesIsIn(pointsArray) == true) {
        $('#vertices').val(markerAsText);
    } else {
        $("#polyError").css("display", "block");
    }
}