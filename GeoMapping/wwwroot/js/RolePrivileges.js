﻿$.ajax({
    url: ENUMS.BaseURL + "RolePrivileges/GetPriveleges",
    type: "GET",
    success: function (result) {
        renderFeatures(result);
    },
    error: function (err) {
        debugger;
        console.log(err);
    }
});

function renderFeatures(privilegesLst) {
    for (var i = 0; i < privilegesLst.length; i++) {
        if (privilegesLst[i].indexOf("Index") !== -1) {
            var entity = privilegesLst[i].substring(0, privilegesLst[i].indexOf("Index") - 1);
            $("#" + entity.toLowerCase()).css("display", "block");
        }
    }
    renderTabs();
}

function renderTabs() {
    var dropdowns = $(".dropdown");
    for (var i = 0; i < dropdowns.length; i++) {
        var children = $(dropdowns[i]).children()[1].children;
        for (var j = 0; j < children.length; j++) {
            var hasVisibleChild = $(children[j]).css('display') == 'block';
            if (hasVisibleChild) {
                break;
            }
        }
        if (!hasVisibleChild) {
            $(dropdowns[i]).css("display", "none");
        }
    }
}

function renderActions(feature) {
    $.ajax({
        url: ENUMS.BaseURL + "RolePrivileges/GetPriveleges",
        type: "GET",
        success: function (result) {
            console.log(result);
            //renderFeatures(result);
            var actions = result.filter(s => s.startsWith(feature));
            debugger;
            for (var i = 0; i < actions.length; i++) {
                var action = actions[i].substring(actions[i].lastIndexOf("/") + 1, actions[i].length);
                $("." + action).css("display", "inline");
            }
        },
        error: function (err) {
            debugger;
            console.log(err);
        }
    });
    
}