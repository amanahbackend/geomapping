﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using GeoMapping.Models;
using GeoMapping.Data;

namespace GeoMapping.Managers
{
	public class SecurityManager
	{
		private UserManager<ApplicationUser> _identityUserManager;
		private IPasswordHasher<ApplicationUser> _passwordHasher;
		private readonly ApplicationDbContext _context;

		public SecurityManager(UserManager<ApplicationUser> identityUserManager,
								IPasswordHasher<ApplicationUser> passwordHasher,
								ApplicationDbContext context)
		{
			_identityUserManager = identityUserManager;
			_passwordHasher = passwordHasher;
			_context = context;
		}



		public async Task<JwtSecurityToken> GetToken(string userName, string password, string jwtSecurityTokenKey, string issuer, string audience)
		{
			ApplicationUser user = _context.Users.Where(x=>x.UserName == userName).FirstOrDefault();
			if (user == null)
			{
				return null;
			}
			var passwordCheckResult = _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, password);
			if (passwordCheckResult == PasswordVerificationResult.Success)
			{
				var userClaims = await _identityUserManager.GetClaimsAsync(user);

				var claims = new[]
					{
						new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
						new Claim(JwtRegisteredClaimNames.Jti,  user.Id),
						new Claim(JwtRegisteredClaimNames.Email, user.Email)
					}.Union(userClaims);

				byte[] jwtSecurityTokenBytes = Encoding.UTF8.GetBytes(jwtSecurityTokenKey);
				var symmetricSecurityKey = new SymmetricSecurityKey(jwtSecurityTokenBytes);
				var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

				var result = new JwtSecurityToken(
						issuer: issuer,
						audience: audience,
						claims: claims,
						expires: DateTime.UtcNow.AddMonths(3),
						signingCredentials: signingCredentials);

				return result;
			}
			else
			{
				return null;
			}
		}
	}
}
