﻿using GeoMapping.Data;
using GeoMapping.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Middlewares
{
	public class AuthorizationMiddleware
	{
		public readonly RequestDelegate _next;

		public AuthorizationMiddleware(RequestDelegate next)
		{
			_next = next;
		}

		//public async Task Invoke(HttpContext httpContext,UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext dbContext, ISecurityManager securityManager)
		//{
		//	string authorizationValue = httpContext.Request.Headers[HeaderNames.Authorization];
		//	string requestedAction = httpContext.Request.Path.Value.ToLower();
		//	if (!requestedAction.Contains("token") &&
		//		!requestedAction.Contains("db") &&
		//		!requestedAction.Contains("register") &&
		//		!requestedAction.ToLower().Contains("KCCVehicle".ToLower()))
		//	{
		//		if (!String.IsNullOrEmpty(authorizationValue))
		//		{
		//			if (authorizationValue.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
		//			{
		//				string token = authorizationValue.Substring("Bearer ".Length).Trim();
		//				JwtSecurityToken jwtTokenObj = new JwtSecurityToken(token);
		//				if (securityManager.IsTokenValid(jwtTokenObj))
		//				{
		//					string username = jwtTokenObj.Payload.Sub;
		//					ApplicationUserManager applicationUserManager = new ApplicationUserManager(userManager);
		//					IList<string> roleNamess = await applicationUserManager.GetRolesAsync(username);

		//					ApplicationRoleManager applicationRoleManager = new ApplicationRoleManager(roleManager);
		//					List<string> userPrivileges = new List<string>();
		//					foreach (var roleName in roleNamess)
		//					{
		//						var privilges = await rolePrivilgeManager.GetPrivelegesByRole(roleName);
		//						userPrivileges.AddRange(privilges);
		//					}

		//					if (!userPrivileges.Contains(requestedAction))
		//					{
		//						// httpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
		//						await _next(httpContext);
		//					}
		//					else
		//					{
		//						await _next(httpContext);
		//					}
		//				}
		//				else
		//				{
		//					httpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
		//				}
		//			}
		//			else
		//			{
		//				//httpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
		//			}
		//		}
		//		else
		//		{
		//			httpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
		//		}
		//	}
		//	else
		//	{
		//		await _next(httpContext);
		//	}
		//}
	}

}
