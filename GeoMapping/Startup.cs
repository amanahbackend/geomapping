﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using GeoMapping.Data;
using GeoMapping.Models;
using GeoMapping.Services;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Mvc.Razor.Compilation;

namespace GeoMapping
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment _env;

        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(provider => Configuration);
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            }).ConfigureApplicationPartManager(manager =>
            {
                var oldMetadataReferenceFeatureProvider = manager.FeatureProviders.First(f => f is MetadataReferenceFeatureProvider);
                manager.FeatureProviders.Remove(oldMetadataReferenceFeatureProvider);
                manager.FeatureProviders.Add(new ReferencesMetadataReferenceFeatureProvider());
            }); ;


            // Add web based authentication service using JWT
            //services.AddAuthentication(options =>
            //{
            //	options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //	options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //}).AddJwtBearer(o =>
            //{
            //	o.TokenValidationParameters = new TokenValidationParameters()
            //	{
            //		ValidIssuer = Configuration["JwtSecurityToken:Issuer"],
            //		ValidAudience = Configuration["JwtSecurityToken:Audience"],
            //		ValidateIssuerSigningKey = true,
            //		IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtSecurityToken:Key"])),
            //		ValidateLifetime = true
            //	};
            //});
        }
        private async Task SeedData(IServiceProvider serviceProvider)
        {
            //adding custom roles
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var UserManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            string[] roleNames = { "SuperAdmin", "Admin", "Agent" };
            IdentityResult roleResult;
            foreach (var roleName in roleNames)
            {
                //creating the roles and seeding them to the database
                var roleExist = await RoleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    roleResult = await RoleManager.CreateAsync(new IdentityRole(roleName));
                }
            }
            //creating a super user who could maintain the web app
            var poweruser = new ApplicationUser
            {
                UserName = Configuration.GetSection("UserSettings")["UserName"],
                Email = Configuration.GetSection("UserSettings")["UserEmail"],
                PhoneNumber = Configuration.GetSection("AgentSettings")["UserPhone"]
            };

            var agentuser = new ApplicationUser
            {
                UserName = Configuration.GetSection("AgentSettings")["UserName"],
                Email = Configuration.GetSection("AgentSettings")["UserEmail"],
                PhoneNumber = Configuration.GetSection("AgentSettings")["UserPhone"]
            };

            string UserPassword = Configuration.GetSection("UserSettings")["UserPassword"];
            string AgentPassword = Configuration.GetSection("UserSettings")["UserPassword"];
            var _user = await UserManager.FindByEmailAsync(Configuration.GetSection("UserSettings")["UserEmail"]);
            var _agent = await UserManager.FindByEmailAsync(Configuration.GetSection("AgentSettings")["UserEmail"]);
            if (_user == null)
            {
                var createPowerUser = await UserManager.CreateAsync(poweruser, UserPassword);
                if (createPowerUser.Succeeded)
                {
                    //here we tie the new user to the "Admin" role 
                    await UserManager.AddToRoleAsync(poweruser, "SuperAdmin");
                }
            }
            if (_agent == null)
            {
                var createAgentUser = await UserManager.CreateAsync(agentuser, AgentPassword);
                if (createAgentUser.Succeeded)
                {
                    //here we tie the new user to the "Admin" role 
                    await UserManager.AddToRoleAsync(agentuser, "Agent");
                }
            }

            ApplicationDbContext _context = serviceProvider.GetRequiredService<ApplicationDbContext>();
            var areaSeeded = _context.Areas.Where(x => x.Polygon == "Random").FirstOrDefault();
            if (areaSeeded == null)
            {
                Area area = new Area()
                {
                    Name = "Random",
                    Polygon = "Random"
                };
                _context.Areas.Add(area);
                _context.SaveChanges();
            }

            var blockSeeded = _context.Blocks.Where(x => x.Polygon == "Random").FirstOrDefault();
            if (blockSeeded == null)
            {
                Block block = new Block()
                {
                    Name = "Random",
                    Polygon = "Random",
                    AreaId = _context.Areas.Where(x => x.Polygon == "Random").FirstOrDefault().Id
                };
                _context.Blocks.Add(block);
                _context.SaveChanges();
            }

            if (!_context.PlotTypes.Any())
            {
                var plotTypes = new List<PlotType>()
                {
                    new PlotType(){Name = "Residential "},
                    new PlotType(){Name = "Commercial "}
                };
                _context.PlotTypes.AddRange(plotTypes);
                _context.SaveChanges();
            }

            var plotSeeded = _context.Plots.Where(x => x.Polygon == "Random").FirstOrDefault();
            if (plotSeeded == null)
            {
                Plot plot = new Plot()
                {
                    Name = "Random",
                    Polygon = "Random",
                    BlockId = _context.Blocks.Where(x => x.Polygon == "Random").FirstOrDefault().Id,
                    PlotTypeId = _context.PlotTypes.FirstOrDefault().Id
                };
                _context.Plots.Add(plot);
                _context.SaveChanges();
            }
            string[] sections = { "Elevators", "Electricity", "Air-Condition" };
            Section sectionSeeded = _context.Sections.Where(x => sections.Contains(x.Name)).FirstOrDefault();
            if (sectionSeeded == null)
            {
                foreach (var section in sections)
                {
                    _context.Sections.Add(new Section()
                    {
                        Name = section
                    });
                    _context.SaveChanges();
                }
            }

            string[] statuses = { "Not Ready", "Land", "Black Building", "Structure", "Finishing" };
            Status statusSeeded = _context.Statuses.Where(x => statuses.Contains(x.Name)).FirstOrDefault();
            if (statusSeeded == null)
            {
                foreach (var status in statuses)
                {
                    _context.Statuses.Add(new Status()
                    {
                        Name = status
                    });
                    _context.SaveChanges();
                }
            }

            if (!_context.Segments.Any())
            {
                List<Segment> segments = new List<Segment>
                {
                    new Segment{ Name = "Greenfield" },
                    new Segment{ Name = "Brownfield" }
                };
                _context.Segments.AddRange(segments);
                _context.SaveChanges();
            }

            if (!_context.SubSegments.Any())
            {
                List<SubSegment> subsegments = new List<SubSegment>
                {
                    new SubSegment{ Name = "Kasima" },
                    new SubSegment{ Name = "Private New" },
                    new SubSegment{ Name = "Private Old" }
                };
                _context.SubSegments.AddRange(subsegments);
                _context.SaveChanges();
            }
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
          .CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                dbContext.Database.Migrate();
            }
            SeedData(serviceProvider).Wait();
        }
    }
}
