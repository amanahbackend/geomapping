﻿using GeoMapping.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMapping.Models.Managers
{
	public class RolePrivilegeManager
	{
		public async Task AddPrivelegToRole(string roleName, string privilegeName, RoleManager<ApplicationRole> identityRoleManager, string userId)
		{
			ApplicationRoleManager applicationRoleManager = new ApplicationRoleManager(identityRoleManager);
			var role = await applicationRoleManager.GetRoleAsyncByName(roleName);

			PrivilgeManager privilgeManager = new PrivilgeManager(Context);
			var privilege = await privilgeManager.GetByAsync(privilegeName);

			base.Add(new RolePrivilge()
			{
				CreatedDate = DateTime.UtcNow,
				UpdatedDate = DateTime.UtcNow,
				FK_CreatedBy_Id = userId,
				FK_ApplicationRole_Id = role.Id,
				FK_Privilge_Id = privilege.Id,
				IsDeleted = false,
				ApplicationRole = role,
				Privilge = privilege
			});
		}

		public async Task<List<string>> GetPrivelegesByRole(string roleName)
		{
			var role = await Context.Roles.FirstOrDefaultAsync(r => r.Name == roleName);
			List<string> result = new List<string>();
			var rolePrivileges = Context.RolePrivilges.Where(rp => rp.FK_ApplicationRole_Id.Equals(role.Id)).ToList();
			List<int> privilegeIds = rolePrivileges.Select(x => x.FK_Privilge_Id).ToList();
			PrivilgeManager privilgeManager = new PrivilgeManager(Context);

			foreach (var privilegeId in privilegeIds)
			{
				result.Add((await privilgeManager.GetByAsync(privilegeId)).Name);
			}
			return result;
		}
	}

}
