﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeoMapping.PACI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace GeoMapping.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Govs")]
    public class GovsController : Controller
    {
        private readonly IConfigurationRoot _configurationRoot;
        public GovsController(IConfigurationRoot configurationRoot)
        {
            _configurationRoot = configurationRoot;
        }

        [HttpGet]
        public IActionResult GetGovs()
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:GovernorateService:ServiceUrl"];

            var result = PACIHelper.GetAllGovernorates(proxyUrl, serviceUrl);
            return Ok(result);
        }
    }
}