﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using GeoMapping.Data;
using GeoMapping.Filters;
using GeoMapping.Managers;
using GeoMapping.Models;
using GeoMapping.Models.AccountViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace GeoMapping.API.Controllers
{
	[Produces("application/json")]
	[Route("api/Auth")]
	public class AuthController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly RoleManager<Role> _roleManager;
		private readonly SignInManager<ApplicationUser> _siginManager;
		private IPasswordHasher<ApplicationUser> _passwordHasher;
		private IConfigurationRoot _configurationRoot;
		private ILogger<AuthController> _logger;
		private readonly ApplicationDbContext _context;

		public AuthController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
			RoleManager<Role> roleManager, IPasswordHasher<ApplicationUser> passwordHasher, IConfigurationRoot configurationRoot
			, ILogger<AuthController> logger, ApplicationDbContext context)
		{
			_userManager = userManager;
			_siginManager = signInManager;
			_roleManager = roleManager;
			_passwordHasher = passwordHasher;
			_configurationRoot = configurationRoot;
			_logger = logger;
			_context = context;
		}


		[AllowAnonymous]
		[HttpPost]
		[Route("register")]
		public async Task<IActionResult> Register([FromBody] RegisterViewModel model)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}
			var user = new ApplicationUser()
			{
				UserName = model.Email,
				Email = model.Email
			};
			var result = await _userManager.CreateAsync(user, model.Password);

			if (result != null)
			{
				return Ok(result);
			}
			return BadRequest();
		}

		[HttpPost]
		[Route("token")]
		public async Task<IActionResult> CreateToken([FromBody] LoginViewModel model)
		{
			try
			{
				SecurityManager securityManager = new SecurityManager(_userManager, _passwordHasher, _context);
				var jwtSecurityTokenKey = _configurationRoot["JwtSecurityToken:Key"];
				var issuer = _configurationRoot["JwtSecurityToken:Issuer"];
				var audience = _configurationRoot["JwtSecurityToken:Audience"];
				var token = await securityManager.GetToken(model.UserName, model.Password, jwtSecurityTokenKey, issuer, audience);
				if (token != null)
				{
					return Ok(new
					{
						Token = new JwtSecurityTokenHandler().WriteToken(token),
						ExpirationTime = token.ValidTo
					});
				}
				else
				{
					return Unauthorized();
				}
			}
			catch (Exception ex)
			{
				_logger.LogError($"error while creating token: {ex}");
				return StatusCode((int)HttpStatusCode.InternalServerError, "error while creating token");
			}
		}
	}
}