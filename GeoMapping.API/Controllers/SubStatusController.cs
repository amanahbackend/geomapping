﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GeoMapping.Data;
using GeoMapping.Models;

namespace GeoMapping.API.Controllers
{
    [Produces("application/json")]
    [Route("api/SubStatus")]
    public class SubStatusController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SubStatusController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/SubStatus
        [HttpGet]
        public IEnumerable<SubStatus> GetSubStatuses()
        {
            return _context.SubStatuses;
        }

        // GET: api/SubStatus/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSubStatus([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subStatus = await _context.SubStatuses.SingleOrDefaultAsync(m => m.Id == id);

            if (subStatus == null)
            {
                return NotFound();
            }

            return Ok(subStatus);
        }

        // PUT: api/SubStatus/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubStatus([FromRoute] int id, [FromBody] SubStatus subStatus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subStatus.Id)
            {
                return BadRequest();
            }

            _context.Entry(subStatus).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubStatusExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SubStatus
        [HttpPost]
        public async Task<IActionResult> PostSubStatus([FromBody] SubStatus subStatus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.SubStatuses.Add(subStatus);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSubStatus", new { id = subStatus.Id }, subStatus);
        }

        // DELETE: api/SubStatus/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSubStatus([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subStatus = await _context.SubStatuses.SingleOrDefaultAsync(m => m.Id == id);
            if (subStatus == null)
            {
                return NotFound();
            }

            _context.SubStatuses.Remove(subStatus);
            await _context.SaveChangesAsync();

            return Ok(subStatus);
        }

        private bool SubStatusExists(int id)
        {
            return _context.SubStatuses.Any(e => e.Id == id);
        }
    }
}