﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GeoMapping.Data;
using GeoMapping.Models;
using Microsoft.AspNetCore.Authorization;
using GeoMapping.Models.ViewModels;
using Newtonsoft.Json;
using GeoMapping.API.Helpers;
using GeoMapping.Controllers;

namespace GeoMapping.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Places")]
    [Authorize]
    public class PlacesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlacesController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("GetPlotPlaces/{plotId}")]
        public List<PlaceViewModel> GetPlotPlaces(int plotId)
        {
            var placesVM = new List<PlaceViewModel>();
            var places = _context.Places
                                    .Where(x => x.PlotId == plotId)
                                    .Include(p => p.Area)
                                    .Include(p => p.Block)
                                    .Include(p => p.Plot)
                                    .Include(p => p.Status)
                                    .ToList();
            for (int i = 0; i < places.Count; i++)
            {
                PlaceViewModel placeViewModel = new PlaceViewModel()
                {
                    Id = places[i].Id,
                    Name = places[i].Name,
                    AreaName = places[i].Area != null ? places[i].Area.Name : "None",
                    PlotName = places[i].Plot != null ? places[i].Plot.Name : "None",
                    Points = JsonConvert.DeserializeObject<Point>(places[i].Point),
                    BlockName = places[i].Block != null ? places[i].Block.Name : "None",
                    PACINo = places[i].PACINo,
                    StatusName = places[i].Status != null ? places[i].Status.Name : "None"
                };
                placesVM.Add(placeViewModel);
            }
            return placesVM;
        }

        [HttpPut]
        [Route("UpdatePlaceLocation")]
        public IActionResult UpdatePlaceLocation([FromBody] PlaceViewModel place)
        {
            var placesVM = new List<PlaceViewModel>();
            var modifyPlace = _context.Places.Where(x => x.Id == place.Id).FirstOrDefault();
            if (modifyPlace != null)
            {
                modifyPlace.Point = Help.ToPointDbStructure(place.Points);
                _context.SaveChanges();
                return Ok();
            }
            return BadRequest();
        }

        // GET: api/Places/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPlace([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var place = await _context.Places.SingleOrDefaultAsync(m => m.Id == id);

            if (place == null)
            {
                return NotFound();
            }

            return Ok(place);
        }

        // PUT: api/Places/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlace([FromRoute] int id, [FromBody] Place place)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != place.Id)
            {
                return BadRequest();
            }

            _context.Entry(place).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlaceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Places
        //[HttpPost]
        //public async Task<IActionResult> PostPlace([FromBody] PlaceAPIViewModel place)
        //{
        //	if (!ModelState.IsValid)
        //	{
        //		return BadRequest(ModelState);
        //	}
        //	string areaName = place.PlaceName + "  Area";
        //	Area area = new Area()
        //	{
        //		Name = areaName,
        //		Polygon = "Random"
        //	};
        //	_context.Areas.Add(area);
        //	_context.SaveChanges();

        //	string plotName = place.PlaceName + "  Plot";
        //	Plot plot = new Plot()
        //	{
        //		Name = plotName,
        //		AreaId = _context.Areas.Where(x => x.Name == areaName).FirstOrDefault().Id,
        //		Polygon = "Random",
        //		AssignedAgentId = Help.GetUserId(User)
        //	};
        //	if (!string.IsNullOrEmpty(place.PACI))
        //	{
        //		plot.PACI_NO = place.PACI;
        //	}
        //	if (!string.IsNullOrEmpty(place.PAHW))
        //	{
        //		plot.PAHW_NO = place.PAHW;
        //	}
        //	_context.Plots.Add(plot);
        //	_context.SaveChanges();

        //	Point position = new Point()
        //	{
        //		Lat = place.Lat,
        //		Lng = place.Lng
        //	};

        //	Place newPlace = new Place()
        //	{
        //		Name = place.PlaceName,
        //		Point = Help.ToPointDbStructure(position),
        //		AreaId = _context.Areas.Where(x => x.Name == areaName).FirstOrDefault().Id,
        //		PlotId = _context.Plots.Where(x => x.Name == plotName).FirstOrDefault().Id,
        //	};

        //	_context.Places.Add(newPlace);
        //	await _context.SaveChangesAsync();

        //	return CreatedAtAction("GetPlace", new { id = newPlace.Id }, place);
        //}

        // DELETE: api/Places/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePlace([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var place = await _context.Places.SingleOrDefaultAsync(m => m.Id == id);
            if (place == null)
            {
                return NotFound();
            }

            _context.Places.Remove(place);
            await _context.SaveChangesAsync();

            return Ok(place);
        }

        private bool PlaceExists(int id)
        {
            return _context.Places.Any(e => e.Id == id);
        }

        [HttpPost]
        [Route("Filter")]
        public List<PlaceViewModel> Filter([FromBody]List<ReportFilter> filters)
        {
            var predicate = PredicateBuilder.True<Place>();
            foreach (var item in filters)
            {
                predicate = predicate.And(PredicateBuilder.CreateEqualSingleExpression<Place>(item.Type, item.Value));
            }
            predicate= predicate.And(p => p.Plot.AssignedAgentId == Help.GetUserId(User));
            var places = _context.Places
                                    .Include(p => p.Area)
                                    .Include(p => p.Block)
                                    .Include(p => p.Status)
                                    .Include(p => p.Plot)
                                    .Where(predicate)
                                    .Select(p => new PlaceViewModel
                                    {
                                        Id = p.Id,
                                        Name = p.Name,
                                        AreaName = p.Area != null ? p.Area.Name : "None",
                                        PlotName = p.Plot != null ? p.Plot.Name : "None",
                                        Points = JsonConvert.DeserializeObject<Point>(p.Point),
                                        BlockName = p.Block != null ? p.Block.Name : "None",
                                        PACINo = p.PACINo,
                                        StatusName = p.Status != null ? p.Status.Name : "None"
                                    }).ToList();

            return places;
        }
    }
}