﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GeoMapping.Data;
using GeoMapping.Models;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;
using GeoMapping.API.Helpers;
using GeoMapping.Models.ViewModels;

namespace GeoMapping.API.Controllers
{
	[Authorize]
	[Produces("application/json")]
	[Route("api/Survey")]
	public class SurveyHistoriesController : Controller
	{
		private readonly ApplicationDbContext _context;

		public SurveyHistoriesController(ApplicationDbContext context)
		{
			_context = context;
		}

		// GET: api/SurveyHistories
		[HttpGet]
		public IEnumerable<SurveyHistory> GetSurveyHistories()
		{
			return _context.SurveyHistories;
		}

		// GET: api/SurveyHistories/5
		[HttpGet("{id}")]
		public async Task<IActionResult> GetSurveyHistory([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var surveyHistory = await _context.SurveyHistories.SingleOrDefaultAsync(m => m.Id == id);

			if (surveyHistory == null)
			{
				return NotFound();
			}

			return Ok(surveyHistory);
		}

		// PUT: api/SurveyHistories/5
		[HttpPut("{id}")]
		public async Task<IActionResult> PutSurveyHistory([FromRoute] int id, [FromBody] SurveyHistory surveyHistory)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != surveyHistory.Id)
			{
				return BadRequest();
			}

			_context.Entry(surveyHistory).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!SurveyHistoryExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return NoContent();
		}

		// POST: api/SurveyHistories
		[HttpPost]
		public IActionResult PostSurveyHistory([FromBody] SurveryViewModel surveyHistory)
		{
			if (!ModelState.IsValid)
			{
				Help.GetUserId(User);
				return BadRequest(ModelState);
			}
 			SurveyHistory survey = new SurveyHistory()
			{
				PlaceId = surveyHistory.PlaceId,
				StatusId = surveyHistory.StatusId,
				CreatedUserId = Help.GetUserId(User),
				CreatedDate = DateTime.Now
			};
			_context.SurveyHistories.Add(survey);
			_context.SaveChanges();

			var savedHistory = _context.SurveyHistories.Where(x => x.CreatedUserId == survey.CreatedUserId).LastOrDefault();
			List<PinSections> list = new List<PinSections>();
			for (int i = 0; i < surveyHistory.Sections.Count; i++)
			{
				var section = surveyHistory.Sections[i];
				PinSections pinSection = new PinSections()
				{
					Competitor = section.Competitor,
					IsAgreed = section.IsAgreed,
					PlaceId = survey.PlaceId,
                    BrandId = section.BrandId,
					SurveyHistoryId = savedHistory.Id,
					SectionId = section.SectionId
				};
				list.Add(pinSection);
			}
			_context.PinSections.AddRange(list);
			_context.SaveChanges();

			return CreatedAtAction("PostSurveyHistory", new { IsSuccess  = true});
		}

		// DELETE: api/SurveyHistories/5
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteSurveyHistory([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var surveyHistory = await _context.SurveyHistories.SingleOrDefaultAsync(m => m.Id == id);
			if (surveyHistory == null)
			{
				return NotFound();
			}

			_context.SurveyHistories.Remove(surveyHistory);
			await _context.SaveChangesAsync();

			return Ok(surveyHistory);
		}

		private bool SurveyHistoryExists(int id)
		{
			return _context.SurveyHistories.Any(e => e.Id == id);
		}
	}
}