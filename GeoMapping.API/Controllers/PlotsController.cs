﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GeoMapping.Data;
using GeoMapping.Models;
using Microsoft.AspNetCore.Authorization;
using GeoMapping.API.Helpers;
using GeoMapping.Models.ViewModels;
using Newtonsoft.Json;

namespace GeoMapping.API.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Plots")]
    public class PlotsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlotsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Plots
        [HttpGet]
        public List<object> GetPlots()
        {
            var plotsVM = new List<object>();
            var plots = _context.Plots
                                .Where(x => x.AssignedAgentId == Help.GetUserId(User))
                                .Include(p => p.Block)
                                    .ThenInclude(b => b.Area)
                                .Include(p => p.PlotType)
                                .ToList();
            for (int i = 0; i < plots.Count; i++)
            {
                int placesNo = _context.Places.Where(x => x.PlotId == plots[i].Id).Count();
                PlotViewModel plotViewModel = new PlotViewModel
                {
                    Id = plots[i].Id,
                    Name = plots[i].Name,
                    //PaciNo = "",
                    PahwNo = plots[i].PAHW_NO,
                    PlacesNo = placesNo,
                    BlockName = plots[i].Block != null ? plots[i].Block.Name : "None",
                    LicenseNumber = plots[i].LicenseNumber,
                    LicenseType = plots[i].LicenseType,
                    OwnerName = plots[i].OwnerName,
                    PlotTypeName = plots[i].PlotType != null ? plots[i].PlotType.Name : "None",
                    Address = plots[i].Address,
                    //AreaId = 1,
                    //AreaName = plots[i].Block != null ? plots[i].Block.Area != null ? plots[i].Block.Area.Name : "" : "",
                    Polygon = plots[i].Polygon != "Random" ? JsonConvert.DeserializeObject<List<Point>>(plots[i].Polygon) : new List<Point>()
                };

                plotsVM.Add(plotViewModel);
            }
            return plotsVM;
        }

        // GET: api/Plots/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPlot([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var plot = await _context.Plots.SingleOrDefaultAsync(m => m.Id == id);

            if (plot == null)
            {
                return NotFound();
            }

            return Ok(plot);
        }

        // PUT: api/Plots/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlot([FromRoute] int id, [FromBody] Plot plot)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != plot.Id)
            {
                return BadRequest();
            }

            _context.Entry(plot).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlotExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Plots
        [HttpPost]
        public async Task<IActionResult> PostPlot([FromBody] Plot plot)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Plots.Add(plot);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPlot", new { id = plot.Id }, plot);
        }

        // DELETE: api/Plots/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePlot([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var plot = await _context.Plots.SingleOrDefaultAsync(m => m.Id == id);
            if (plot == null)
            {
                return NotFound();
            }

            _context.Plots.Remove(plot);
            await _context.SaveChangesAsync();

            return Ok(plot);
        }

        private bool PlotExists(int id)
        {
            return _context.Plots.Any(e => e.Id == id);
        }
    }
}