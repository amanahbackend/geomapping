﻿using GeoMapping.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GeoMapping.API.Helpers
{
	public static class Help
	{
		public static string GetUserId(ClaimsPrincipal User)
		{
			return User.FindFirst("jti")?.Value;
		}
		public static string ToPointDbStructure(Point point)
		{
			var p = string.Format("{0}lat{0}:{1},{0}lng{0}:{2}", '"', point.Lat, point.Lng);
			return "{" + p + "}";
		}
	}
}
