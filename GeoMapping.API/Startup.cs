﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeoMapping.Data;
using GeoMapping.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;

namespace GeoMapping.API
{
	public class Startup
	{
		public IConfigurationRoot Configuration { get; }
		private IHostingEnvironment _env;

		public Startup(IHostingEnvironment env)
		{
			_env = env;
			var builder = new ConfigurationBuilder()
			  .SetBasePath(env.ContentRootPath)
			  .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
			  .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
			  .AddEnvironmentVariables();
			Configuration = builder.Build();
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddSingleton<IConfigurationRoot>(provider => Configuration);

			services.AddMvc().AddJsonOptions(options =>
			{
				options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
			});

			services.AddDbContext<ApplicationDbContext>(options =>
					  options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

			services.ConfigureApplicationCookie(cfg =>
			{
				cfg.Events = new CookieAuthenticationEvents
				{
					OnRedirectToLogin = ctx =>
					{
						if (ctx.Request.Path.StartsWithSegments("/api"))
						{
							ctx.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
						}
						return Task.FromResult(0);
					}
				};
			})
		   .AddIdentity<ApplicationUser, Role>()
		   .AddEntityFrameworkStores<ApplicationDbContext>()
		   .AddDefaultTokenProviders();

			// Add web based authentication service using JWT
			services.AddAuthentication(options =>
			{
				options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			}).AddJwtBearer(o =>
			{
				o.TokenValidationParameters = new TokenValidationParameters()
				{
					ValidIssuer = Configuration["JwtSecurityToken:Issuer"],
					ValidAudience = Configuration["JwtSecurityToken:Audience"],
					ValidateIssuerSigningKey = true,
					IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtSecurityToken:Key"])),
					ValidateLifetime = true
				};
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			loggerFactory.AddConsole(Configuration.GetSection("Logging"));
			loggerFactory.AddDebug();
			app.UseAuthentication();


			//using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
			//  .CreateScope())
			//{
			//	var dbContext = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
			//	dbContext.Database.Migrate();
			//}

			//app.UseMiddleware<AuthorizationMiddleware>();

			app.UseMvc();
		}
	}
}
