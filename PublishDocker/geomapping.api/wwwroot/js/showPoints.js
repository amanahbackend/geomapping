﻿

function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		center: { lat: 29, lng: 48 },
		zoom: 9,
		mapTypeId: 'terrain'
	});

	// Create an array of alphabetical characters used to label the markers.
	var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

	// Add some markers to the map.
	// Note: The code uses the JavaScript Array.prototype.map() method to
	// create an array of markers based on a given "locations" array.
	// The map() method here has nothing to do with the Google Maps API.
	var markersLoc = [];
	var infoWin = new google.maps.InfoWindow();

	locations.sort(function (a, b) {
		var nameA = a.plotName.toLowerCase(), nameB = b.plotName.toLowerCase()
		if (nameA < nameB) //sort string ascending
			return -1
		if (nameA > nameB)
			return 1
		return 0 //default return value (no sorting)
	})
	var index = 0;
	for (var i = 0; i < locations.length; i++) {
		if (i == 0) {
			index = 0;
		} else {
			if (locations[i].plotName != locations[i - 1].plotName) {
				index += 1;
			}
		}
		markersLoc.push({
			location: { lat: parseFloat(locations[i].points.lat), lng: parseFloat(locations[i].points.lng) }, name: locations[i].name, plotName: locations[i].plotName, areaName: locations[i].areaName, index: index
		});
	}

	var markers = markersLoc.map(function (mark, i) {
		console.log(mark);
		var marker = new google.maps.Marker({
			label: labels[mark.index % labels.length],
			position: mark.location
		});
		google.maps.event.addListener(marker, 'click', function (evt) {
			var content = "<p>Name: " + mark.name + "</p>" +
				"<p>Plot Name: " + mark.plotName + "</p>" +
				"<p>Area Name: " + mark.areaName + "</p>";
			infoWin.setContent(content);
			infoWin.open(map, marker);
		})
		return marker;
	});
	// Add a marker clusterer to manage the markers.
	var markerCluster = new MarkerClusterer(map, markers,
		{ imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
}