﻿var markers = [];
function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		center: { lat: 29, lng: 48 },
		zoom: 10
	});

	var drawingManager = new google.maps.drawing.DrawingManager({
		drawingMode: google.maps.drawing.OverlayType.MARKER,
		drawingControl: false,
	});
	drawingManager.setMap(map);
	google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
		markers.push({
			lat: event.overlay.position.lat(),
			lng: event.overlay.position.lng()
		});
		overlayClickListener();
		//drawingManager.setDrawingMode(null);
	});
}
function overlayClickListener() {
	var point;
	var polygons = "";

	//var lat = overlay.position.lat();
	//var lng = overlay.position.lng();

	//point = '{ "lat": "' + lat + '", "lng": "' + lng + '" }';
	//if (i != polygon.length - 1) {
	//	polygons += ',';
	//}
	var markerAsText = JSON.stringify(markers);

	console.log(markers);
	
	$('#vertices').val(markerAsText);
}