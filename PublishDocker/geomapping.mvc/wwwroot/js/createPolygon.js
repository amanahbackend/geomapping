﻿function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		center: { lat: 29, lng: 48 },
		zoom: 10
	});

	var drawingManager = new google.maps.drawing.DrawingManager({
		drawingMode: google.maps.drawing.OverlayType.POLYGON,
		drawingControl: false,
	});
	drawingManager.setMap(map);
	google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
		drawingManager.setDrawingMode(null);
		overlayClickListener(event.overlay);
	});
}
function overlayClickListener(overlay) {
	google.maps.event.addListener(overlay, "mouseup", function (event) {
		var polygon;
		var polygons = "";
		if ($('#vertices').val() == "") {
			polygon = overlay.getPath().getArray();
			console.log("p");
			polygons += "["
			for (var i = 0; i < polygon.length; i++) {
				polygons += '{ "lat": "' + polygon[i].lat() + '", "lng": "' + polygon[i].lng() + '" }';
				if (i != polygon.length - 1) {
					polygons += ',';
				}
			}
			polygons += "]";
		} else {
			console.log("p2");
			console.log(overlay.getPath().getArray());
			polygon = overlay.getPath().getArray();
			polygons += "["
			for (var i = 0; i < polygon.length; i++) {
				polygons += '{ "lat": "' + polygon[i].lat() + '", "lng": "' + polygon[i].lng() + '" }';
				if (i != polygon.length - 1) {
					polygons += ',';
				}
			}
			polygons += "]";
			polygons = $('#vertices').val() + "|" + polygons;
		}
		$('#vertices').val(polygons);
	});
}